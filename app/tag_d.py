# coding: utf8
#Lecteur d'arguments
import argparse
#Parcours de fichiers
import os
import os.path
#Expressions régulières
import re
#On récupère les autres scripts
from subscripts import *
#Copie d'arborescence
from distutils.dir_util import copy_tree
#Creation d'archive et supresson de fichiers
import shutil
import bs4



"""
tag(itempath,norec,inplace,lang,exts,keepstruct,check_result,results,ana)
Cette fonction permet de lancer la fonction principale de tagging.
Entrees:
itempath: Chemin d'accès relatif vers l'objet à traiter(string)
lang: Indique la langue de TreeTagger que l'utilisateur veut lancer (string)
exts: Indique la ou les extensions de fichiers sur lesquels le script doit se lancer (list)
inplace: Indique si le traitement des fichiers XML doit se faire dans une copie
keepstruct: Indique la balise qui doit être utilisée comme balise minimum pour
    l'analyse avancée XML (string)
    du fichier original au format XML (boolean)
check_result: Résultats de l'analyse des informations données par l'utilisateur (tuple)
results: Tableau contenant les résulats des étiqueteurs (list)
ana: Indique si l'utilisateur souhaite lancer AnaText sur les résultats (boolean)
Sortie:
Création d'un ou de plusieurs fichiers contenant les résultats demandés

"""
def tag(itempath,norec,inplace,lang,exts,keepstruct,check_result,results,ana):
    #Lecture des informations contenues dans la variable des résulats
    if check_result[0]in ["TTG","TAL","NLP"]:
        prog=check_result[0]
        path=check_result[1]
        valid_langs=check_result[2]
    elif check_result[0]=="UDP":
        prog=check_result[0]
        path=check_result[1]
        valid_langs=check_result[2]
        models=check_result[3]
    elif check_result[0]=="SPC":
        prog=check_result[0]
        valid_langs=check_result[1]
        #Si l'objet actuel est un dossier, on parcourt chacun de ces élements
        #et on appelle cette fonction recursivement sur eux
    try:
        if os.path.isdir(itempath):
            for item in os.listdir(itempath):
                newitem=itempath+"/"+item
                #Si l'option "pas de récursivité" est activée, on ne fait un appel
                #récursif que si l'objet courant est un fichier et non un dossier
                if norec:
                    if os.path.isfile(newitem):
                        tag(newitem,norec,inplace,lang,exts,keepstruct,check_result,result,ana)
                else:
                    tag(newitem,norec,inplace,lang,exts,keepstruct,check_result,results,ana)
        elif os.path.isfile(itempath):
            try:
                #Selon le programme choisi, il est nécessaire de préciser certains arguments
                #supplémentaires
                if prog=="SPC":
                    run_tagger(itempath,prog,lang,exts,inplace,keepstruct,results,valid_langs,ana)
                elif prog=="UDP":
                    run_tagger(itempath,prog,lang,exts,inplace,keepstruct,results,valid_langs\
                    ,ana,path=path,models=models)
                elif prog in ["TAL","TTG","NLP"]:
                    run_tagger(itempath,prog,lang,exts,inplace,keepstruct,results,valid_langs\
                    ,ana,path=path)
            except Exception as e:
                raise
    except Exception as e:
        raise

#Définition des arguments de la ligne de commande
parser = argparse.ArgumentParser(description='Etiquettage de textes avec TreeTagger')
#Récuperation du dossier ou fichier d'entrée
parser.add_argument('input', metavar='entree', type=str, \
help='Nom du fichier (ou dossier) à traiter (relatif au dossier input)')
#Récuperation du dossier ou fichier de sortie
parser.add_argument('output', metavar='sortie', type=str, \
help='Nom du fichier (ou dossier avec l\'option --sep) à créer (relatif au dossier output)')
#Récuperation du paramètre de séparation des résultats en plusieurs fichiers
parser.add_argument('--sep', action="store_true" , \
help='Crée un fichier par résulat (Par défaut: agglutination des résultats en un fichier)')
#Récuperation du paramètre de non-récursivité
parser.add_argument('--norec', action="store_true" , \
help='Effectue un parcours non récursif de l\'entree même si c\'est un dossier\
(Par défaut: parcours récursif de l\'entrée si c\'est un dossier)')
#Récuperation du paramètre de langue
parser.add_argument('--lang' , type=str, default="fra", \
help='Indiquer le code ISO 639-3 de la langue à analyser (Par défaut: "fra")')
#Récuperation du paramètre des extensions
parser.add_argument('--exts' , type=str, default=[".txt"], nargs="+", \
help='Indiquer la ou les extensions de fichier à analyser (Par défaut: ".txt" seulement)')
#Récuperation de l'analyseur à utiliser
parser.add_argument('--prog' , type=str, default=["Treetagger"], nargs="+", \
help='Indiquer le ou lesquels analyseurs morphosyntaxiques utiliser (Par défaut: "Treetagger")\
(Une liste des analyseurs possibles est disponible dans la documentation)')
#Option de traitement XML en place
parser.add_argument('--inplace', action="store_true" , \
help='Inscrit les résultats de l\'analyse d\'un fichier XML sous forme d\'un \
 élement XML ajouté à une copie du fichier original.(Aucun effet sur les fichiers non-xml)\
(Par défaut: Inscrit les résultats sous le format de texte de l\'analyseur utilisé)')
#Option pour la création de fichiers de sortie propres
parser.add_argument('--clean', action="store_true" , \
help='Enlève les séparateurs entre le résultat de chaque fichier d\'entrée \
(Par défaut: Un séparateur est inséré entre le résultat de chaque fichier)')
#Option de traitement XML qui conserve la structure
parser.add_argument('--keepstruct' , type=str, default="n/a", \
help='Incrit les resultats dans une copie du fichier XML entré original, en utilisant\
 la balise donnée comme élément minimal (toute balise subordonée sera ignorée)\
 (Par défaut: pas de traitement spécial) ')
#Option pour le traitement à l'aide d'AnaText
parser.add_argument('--ana', action="store_true" , \
help='Crée un fichier .zip dans la sortie contenant les\
 résultats Anatext du fichier.  Ne fonctionne pas si les options "keepstruct" ou\
 "inplace" sont activées. Si plusieurs outils sont activés, les résulats seront\
 regroupés en un seul fichier. Cet option a besoin de convertir les étiquettes\
 morphosyntaxique apposées par les différents outils afin qu\'elles correspondent\
 à celles de TreeTagger. Il est donc possible que les résulats perdent en précision.')




#On récupère les arguments renseignés par l'utilisateur
args=parser.parse_args()
if args.ana==True:
    #Désactive le traitement par AnaText si l'utilisateur a précisé l'une des options de traitement spécial du xml
    if args.keepstruct!="n/a" or args.inplace:
        args.ana=False


#On récupère le chemin d'accès à TreeTagger depuis le fichier de configuration
try:
    config=open("config.conf",mode="r",encoding="UTF-8")
except Exception as e:
    print("Fichier de configuration non trouvé")

#Lecture du fichier de configuration
try:
    lines=[]
    configs={}
    for line in config.readlines():
        if line[0]!="#":
            if line!="\n":
                lines.append(line.split(":"))
    for line in lines:
        if line[0] not in configs.keys():
            if line[0]!="UDPipe_Lang":
                configs[line[0]]=line[1]
            else:
                ligne=line[1].split("|")
                ligne[0]="UDPipe_Lang_"+ligne[0]
                configs[ligne[0]]=ligne[1]
        else:
            raise Exception("Fichier de configuration mal formé")
    try:
        #On crée une liste ou l'on va stocker tous les resultats sous la forme de paires
        #au format ("nom_de_fichier_original","resultats_TreeTagger")
        results=[]
        decor="|===============================================================================|"
        #On vérifie la validité des informations entrées par l'utilisateur,
        #et on extrait toutes ces informations pour les stocker dans une variable
        args.input="input/"+args.input
        if not args.ana:
            args.output="output/"+args.output
        #On ajoute automatiquement .xml à la liste des extensions à analyser
        #si l'utilisateur renseigne l'option inplace
        if args.inplace and ".xml" not in args.exts:
            args.exts.append(".xml")
        for prog in args.prog:
            try:
                check_result=check_parameters(args.lang,prog,configs)
            except Exception as e:
                raise
            #On fait l'appel initial de la fonction
            tag(args.input,args.norec,args.inplace,args.lang,args.exts,args.keepstruct,check_result,results,args.ana)
        #On enregistre les résultats dans des fichiers séparés si l'option --sep est renseigneé
        #par l'utilisateur, sinon on assemble tous les résultats en un seul fichier.
        if len(results)>0:
            if args.sep:
                try:
                    #Pour nommer le nouveau dossier, on prend l'option de sortie donnée par
                    #l'utilisateur et on enlève tout éventuelle extension de fichier
                    args.output=re.sub("\.\w+","",args.output)
                    os.mkdir(args.output)
                    for item in results:
                        #Ici, le format .txt est choisi par defaut, car c'est le plus adapté
                        #pour les sorties de la plupart des analyseurs

                        #On crée une copie de l'arborescence originale
                        chemin_fichier=item[0].split("/")
                        #Si la longueur est 2, c'est un fichier isolé, pas besoin
                        #de sous fichiers.
                        if len(chemin_fichier)>2:

                            chemin_a_creer="/".join(chemin_fichier[1:-1])
                            chemin_a_creer=args.output+"/"+chemin_a_creer
                            os.makedirs(chemin_a_creer, exist_ok=True)
                            output=open(chemin_a_creer+"/"+chemin_fichier[-1]+"_sortie_"+item[2]+".txt",mode="w",encoding="UTF-8")
                        else:
                            output=open(args.output+"/"+chemin_fichier[-1]+"_sortie_"+item[2]+".txt",mode="w",encoding="UTF-8")
                        if args.clean:
                            output.write(item[1]+"\n\n")
                        else:
                            output.write(decor+"\n"+"Sortie "+item[2]+" de "+item[0]+"\n"+decor+"\n"+item[1]+"\n\n")
                        output.close()
                except Exception as e:
                    raise Exception("Dossier déja existant")
            #Si l'utilisateur a renseigné l'option ana, on crée le fichier zip contenant le résultat
            elif args.ana:
                copy_tree("anatext_vide","anatext")
                zipname=args.output+"_ana"
                args.output="anatext/"+args.output
                output=open(args.output,mode="w",encoding="UTF-8")
                for item in results:
                    output.write(item[1]+"\n\n")
                output.close()
                subprocess.run(["perl","anatext/ttg2stats.pl",args.output])
                for item in os.listdir("./anatext"):
                    if not item.endswith(".pl"):
                        newitem="./anatext"+"/"+item
                        if os.path.isfile(newitem):
                            if newitem.endswith(".html"):
                                resultspath=item
                                shutil.copy2(newitem,"./out-html/")
                            else:
                                shutil.copy2(newitem,"./out-html/anatext")
                shutil.make_archive("output/"+zipname,"zip","anatext")
                shutil.rmtree("anatext", ignore_errors=True)

            else:
                output=open(args.output,mode="w",encoding="UTF-8")
                for item in results:
                    if args.clean:
                        output.write(item[1]+"\n\n")
                    else:
                        output.write(decor+"\n"+"Sortie "+item[2]+" de "+item[0]+"\n"+decor+"\n"+item[1]+"\n\n")
                output.close()
    except Exception as e:
        print("Traitement interrompu à cause d'une erreur:",e)
except Exception as e:
    print("Traitement interrompu à cause d'une erreur :",e)
    
