// module de js de AnaText : dépendance : jQuery et JQuery.dataTables
// traduction du tagset vers treetagger
var txt = text;
var tradCat = {
  "NOM": "(NOM|NAM)",
  "VER": "VER",
  "ADJ": "ADJ",
  "ADV": "ADV",
  "DET": "DET",
  "PRO": "PRO",
  "PRE": "PRP",
  "ART": "DET",
  "CON": "KON",
  "PON": "PUN",
  "PHR": "SENT",
  "TOK": "[^_ ]+",
  "NOSENT": "(?!SENT)[^_ ]+"
};
var concordTable = false; // Booléen indiquant si la table concordTable a déjà été créé avec dataTables
var coocTable = false; // Booléen indiquant si la table coocTable a déjà été créé avec dataTables
var patternTable = false; // Booléen indiquant si la table concordTable a déjà été créé avec dataTables
var segRepTable = false; // Booléen indiquant si la table concordTable a déjà été créé avec dataTables
var pageLength = 1200;
var pageTot = Math.floor(txt.length / pageLength) + 1;
var pageNum = 1;

function calcMotif(pattern, motEntier, useLemma) {
  // les car ( et | {} sont réservés à l'ensemble du pattern, à l'extérieur des tokens
  // les car + * . ? sont utilisable dans le token
  // pas de look behind, on doit ajouter un esp pour splitter derrière ( et |
  pattern = pattern.replace(/\(/, "( ");
  pattern = pattern.replace(/\|/, "| ");
  var toks = pattern.split(/(?=[\)\|])|(?=\{\d*,?\d*\})| /);
  var motif = "";
  for (var i in toks) {
    var tok = toks[i];
    if (tok == "") {
      continue;
    }

    // attention, étant donnée les répétitions, pour chaque token, il faut intégrer l'espace en début de token (esp ou ^)
    if (tradCat[tok]) {
      motif += "(?:[ ^][^_ ]+_[^_]+_" + tradCat[tok] + ')';
    } else {
      var l = tok.split(/_/);
      var lemForm = l[0];
      var cat = '[^_ ]+'
      if (l[1]) {
        cat = l[1];
        if (tradCat[cat]) {
          cat = tradCat[cat];
        }
      }

      if (tok.match(/[\(\)|]|\{\d*,?\d*\}/)) {
        motif += tok;
      } else {
        //~ if (tok.match(/[.?]/)) {
        //~ tok="\\"+tok;
        //~ }
        if (lemForm.match(/(^|[^\\])[.]/)) {
          // le caractère . doit être remplacé par [^_]
          lemForm = lemForm.replace(/(^|[^\\])[.]/, "$1[^_ ]");
        }
        if (motEntier) {
          if (useLemma) {
            motif += "(?: [^_ ]+_" + lemForm + "_" + cat + ")";
          } else {
            motif += "(?: " + lemForm + "_[^_ ]+_" + cat + ")";
          }
        } else {
          if (useLemma) {
            motif += "(?: [^_ ]+_[^_ ]*" + lemForm + "[^_ ]*_" + cat + ")";
          } else {
            motif += "(?: [^_ ]*" + lemForm + "[^_ ]*_[^_ ]+_" + cat + ")";
          }

        }
      }
    }
  }

  motif = motif.replace(/^ /, "");
  motif = motif.replace(/\| /, "|");

  return motif;
}

// recherche de concordance
function searchConcord(pattern) {
  var motEntier = $("#motEntier")[0].checked;
  var caseSensitive = $("#caseSensitive")[0].checked;
  var useLemma = $("#useLemmaConcord")[0].checked;
  var wordspan = $("#wordspan").val();

  var insensitive = caseSensitive ? "" : "i";

  var motif = calcMotif(pattern, motEntier, useLemma);

  // d'abord suppression si besoin est
  if (concordTable) {
    $('#concordTable').dataTable().fnDestroy();
    $('#concordTable').remove();
    $('#concordTableDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="concordTable"></table>');
  }

  // construction du tableau contenant les contextes g et d
  // attention, la regexp plante si on commance par chercher (?:[^_ ]+_[^_ ]+_[^_ ]+ ){0,"+wordspan+"})
  //
  //alert("/("+motif+")/g"+insensitive);
  var regexpGlobal = new RegExp("(" + motif + ")", "g" + insensitive);
  //var founds=text.match(regexpGlobal);
  var tabResult = [];
  var contextLeft = new RegExp("(([^_ ]+_[^_ ]+_[^_ ]+ ?){0," + wordspan + "})$");
  var contextRight = new RegExp("^ (([^_ ]+_[^_ ]+_[^_ ]+ ?){0," + wordspan + "})");
  // !!!!! pour avoir des stat exactes, on n'inclut pas les contextes dans le matching
  //alert(text);
  while (regexpGlobal.test(text)) {
    //alert(regexpGlobal.lastIndex+":"+ RegExp.$1);
    var pivot = RegExp.$1;
    var pos = regexpGlobal.lastIndex;
    var leftSpanIndex = pos - pivot.length - wordspan * 20;
    var rightSpanIndex = pos;
    var leftSpanLength = wordspan * 20;
    if (leftSpanIndex < 0) {
      leftSpanIndex = 0;
      leftSpanLength = pos - pivot.length;
    }
    var leftSpan = text.substr(leftSpanIndex, leftSpanLength);
    var rightSpan = text.substr(rightSpanIndex, wordspan * 20);
    var left = leftSpan;
    var right = rightSpan;

    if (leftSpan.match(contextLeft)) {
      left = RegExp.$1;
    }
    if (rightSpan.match(contextRight)) {
      right = RegExp.$1;
    }
    position = Math.round(pos / text.length * 10000) / 100 + "%";
    l = left.replace(/_[^_]+_[^_ ]+/g, "");
    p = pivot.replace(/_[^_]+_[^_ ]+/g, "");
    r = right.replace(/_[^_]+_[^_ ]+/g, "");
    tabResult.push([position, l, p, r]);
    //alert(regexpGlobal.lastIndex+":"+ RegExp.$1 + RegExp.$2);
  }

  //var regexpGlobal=new RegExp("((?:[^_ ]+_[^_ ]+_[^_ ]+ ){0,"+wordspan+"})("+motif+")((?: [^_ ]+_[^_ ]+_[^_ ]+){0,"+wordspan+"})","g"+insensitive);	// cette fois on utilise les parenthèse capturantes
  //var regexp=new RegExp("^((?:[^_ ]+_[^_ ]+_[^_ ]+ ){0,"+wordspan+"})("+motif+")((?: [^_ ]+_[^_ ]+_[^_ ]+){0,"+wordspan+"})$",insensitive);	// cette fois on utilise les parenthèse capturantes
  //if (founds) {
  //	for (var i=0;i<founds.length;i++) {
  //		alert(founds[i].index);
  //		if (founds[i].match(regexp)) {
  //			var left=RegExp.$1;
  //			var pivot=RegExp.$2;
  //			var right=RegExp.$3;

  //			l=left.replace(/_[^_]+_[^_ ]+/g,"");
  //			p=pivot.replace(/_[^_]+_[^_ ]+/g,"");
  //			r=right.replace(/_[^_]+_[^_ ]+/g,"");
  //			tabResult.push([l,p,r]);
  //		}
  //	}
  //} else {
  //	alert("not found");
  //	return;
  //}

  var n = tabResult.length;
  $("#nbocc").html(n);

  // construction du tableau avec dataTable
  $('#concordTable').dataTable({
    dom: 'T<"clear">lfrtip',
    "aaData": tabResult,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    "aoColumnDefs": [{
        "sWidth": "31%",
        "aTargets": [1, 2, 3]
      },
      {
        "sWidth": "7%",
        "aTargets": [0]
      }
    ],
    "aoColumns": [{
        "sTitle": "Position",
        "sClass": "displayPage clickable"
      },
      {
        "sTitle": "Contexte gauche",
        "sClass": "right"
      },
      {
        "sTitle": "Pivot",
        "sClass": "center"
      },
      {
        "sTitle": "Contexte droit",
        "sClass": "left"
      }
    ]
  });
  concordTable = true;
}

// recherche de cooccurrence
function searchCooc(pattern) {
  var fenLeft = $("#fenLeft").val();
  var fenRight = $("#fenRight").val();
  var caseSensitive = $("#caseSensitiveCooc")[0].checked;
  var useLemma = $("#useLemmaCooc")[0].checked;
  var motEntier = $("#motEntierCooc")[0].checked;
  var insideSent = $("#insideSent")[0].checked;

  var insensitive = caseSensitive ? "" : "i";

  var motif = calcMotif(pattern, motEntier, useLemma);

  var notSent = "";
  if (insideSent) {
    notSent = "(?!SENT)";
  }
  var regexpStr = "((?:[^_ ]+_[^_ ]+_" + notSent + "[^_ ]+ ?){0," + fenLeft + "})(" + motif + ")((?: [^_]+_[^_ ]+_" + notSent + "[^_ ]+){0," + fenRight + "})";
  console.log("regex :" + regexpStr);
  var regexp = new RegExp(regexpStr, "g" + insensitive);

  // d'abord suppression si besoin est
  if (coocTable) {
    $('#coocTable').dataTable().fnDestroy();
    $('#coocTable').remove();
    $('#coocTableDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="coocTable"></table>');
  }

  // recherche du pattern
  var founds = text.match(regexp);
  if (founds) {

    // construction du tableau contenant tous les collocatifs
    //regexp=new RegExp("^(([^_ ]+_[^_ ]+_[^_ ]+ ?){0,"+fen+"}) .*? (( ?[^_]+_[^_ ]+_[^_ ]+){0,"+fen+"})$");	// cette fois on utilise les parenthèse capturantes
    var collocs = [];

    var freqPivot = founds.length;
    for (var i = 0; i < founds.length; i++) {
      console.log("found : " + founds[i]);
      founds[i].match(regexp);
      var left = RegExp.$1;
      var right = RegExp.$3;
      //~ //alert(founds[i]+"   Left = ["+left+"] right=["+right+"]");

      var context = left.split(/ /);
      console.log("Left: " + context.length.toString());
      context = context.concat(right.split(/ /));
      console.log("Left+Right: " + context.length.toString());
      for (var j in context) {
        if (context[j] != "" && !context[j].match(/_(NUM|PUN|SENT)/)) {
          collocs.push([context[j].replace(/[^_ ]+_([^_]+)_[^_ ]+/g, "$1"), context[j].replace(/[^_ ]+_[^_]+_([^_ ]+)/g, "$1")]);
        }
      }
    }

    // calcul du hachage colloc_freq
    var colloc_freq = [];
    for (var i in collocs) {
      colloc = collocs[i][0] + "__" + collocs[i][1];

      if (!colloc_freq[colloc]) {
        colloc_freq[colloc] = 0;
      }
      colloc_freq[colloc]++;
    }

    // calcul du tableau tabResult
    var tabResult = [];

    for (var colloc in colloc_freq) {
      //alert(colloc);
      lemme_cat = colloc.split(/__/);
      lemme = lemme_cat[0];
      cat = lemme_cat[1];
      var ams = computeAM(freqPivot, lemme2freq[lemme], colloc_freq[colloc], nbOccTot);
      var ll = Math.round(1000 * ams[0]) / 1000;
      var tscore = Math.round(1000 * ams[1]) / 1000;
      var im = Math.round(1000 * ams[3]) / 1000;
      tabResult.push([lemme, cat, colloc_freq[colloc], ll, tscore, im]);
      //~ alert(colloc+"->"+colloc_freq[colloc]);
    }

    // construction du tableau avec dataTable
    $('#coocTable').dataTable({
      dom: 'T<"clear">lfrtip',
      "aaData": tabResult,
      "aaSorting": [
        [3, "desc"]
      ],
      "aLengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "Tous"]
      ],
      "aoColumnDefs": [{
        "sWidth": "80px",
        "aTargets": [0, 1, 2, 3, 4, 5]
      }],
      "aoColumns": [{
          "sTitle": "Collocatif",
          "sClass": "center",
          "sClass": "clickable"
        },
        {
          "sTitle": "Cat",
          "sClass": "center"
        },
        {
          "sTitle": "Cooccurrences",
          "sClass": "center"
        },
        {
          "sTitle": "LogLike",
          "sClass": "center"
        },
        {
          "sTitle": "Im",
          "sClass": "center"
        },
        {
          "sTitle": "t-score",
          "sClass": "center"
        }
      ]
    });
    coocTable = true;
  } else {
    $('#coocTable').dataTable({
      dom: 'T<"clear">lfrtip',
      "aaData": [],
      "aaSorting": [
        [3, "desc"]
      ],
      "aLengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "Tous"]
      ],
      "aoColumnDefs": [{
        "sWidth": "80px",
        "aTargets": [0, 1, 2, 3, 4, 5]
      }],
      "aoColumns": [{
          "sTitle": "Collocatif",
          "sClass": "center",
          "sClass": "clickable"
        },
        {
          "sTitle": "Cat",
          "sClass": "center"
        },
        {
          "sTitle": "Cooccurrences",
          "sClass": "center"
        },
        {
          "sTitle": "LogLike",
          "sClass": "center"
        },
        {
          "sTitle": "Im",
          "sClass": "center"
        },
        {
          "sTitle": "t-score",
          "sClass": "center"
        }
      ]
    });
  }

}

// calcul des mesures d'association
function computeAM(n1, n2, n12, N) {

  var ePP = n1 * n2 / N;
  var ePA = n1 * (N - n2) / N;
  var eAP = (N - n1) * n2 / N;
  var eAA = (N - n1) * (N - n2) / N;

  if (n12 * n1 * n2 * N == 0) {
    alert("Anomalie : n1=" + n1 + ", n2=" + n2 + ", n12=" + n12 + ", N=" + N + "\n");
    return [0, 0, 0, 0];
  }

  var lPP = n12 * Math.log(n12 / ePP) / Math.LN2;
  var lPA = (n1 - n12 > 0) ? (n1 - n12) * Math.log((n1 - n12) / ePA) / Math.LN2 : 0;
  var lAP = (n2 - n12 > 0) ? (n2 - n12) * Math.log((n2 - n12) / eAP) / Math.LN2 : 0;
  var lAA = (N - n1 - n2 + n12 > 0) ? (N - n1 - n2 + n12) * Math.log((N - n1 - n2 + n12) / eAA) / Math.LN2 : 0;

  var ll = 2 * (lPP + lPA + lAP + lAA);
  var tscore = (n12 - ePP) / Math.sqrt(ePP);
  var zscore = (n12 - ePP) / Math.sqrt(n12);
  var im = Math.log((n12 * N) / (n1 * n2)) / Math.LN2;

  //~ alert( "n1="+n1+", n2="+n2+", n12="+n12+", N="+N+"\n");
  //~ alert( "ll="+ll+", tscore="+tscore+", zscore="+zscore+", im="+im+"\n");
  return [ll, tscore, zscore, im];
}

// recherche de patterns syntaxiques
function searchPattern(pattern) {
  var useLemma = $("#useLemmaSearch")[0].checked;
  var motEntier = $("#motEntierSearch")[0].checked;
  var caseSensitive = $("#caseSensitiveSearch")[0].checked;

  var motif = calcMotif(pattern, motEntier, useLemma);

  var regexp = new RegExp(motif, "g");

  var founds = text.match(regexp);
  var stats = [];
  if (founds) {
    for (var i = 0; i < founds.length; i++) {
      if (!stats[founds[i]]) {
        stats[founds[i]] = 0;
      }
      stats[founds[i]]++;
    }
  }

  var tabResult = [];
  for (var match in stats) {
    var s = match.replace(/_[^_ ]+_[^_ ]+/g, "");
    s = s.replace(/^ /, "");
    var l = s.split(/ /);
    tabResult.push([s, stats[match]]);
  }

  // construction du tableau avec dataTable
  // d'abord suppression si besoin est
  if (patternTable) {
    $('#patternTable').dataTable().fnDestroy();
    $('#patternTable').remove();
    $('#patternTableDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="patternTable"></table>');
  }
  $('#patternTable').dataTable({
    dom: 'T<"clear">lfrtip',
    "aaData": tabResult,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sTitle": "Segment",
        "sClass": "searchConcordNoLemma clickable"
      },
      {
        "sTitle": "Fréquence",
        "sClass": "center"
      }
    ]
  });
  patternTable = true;


}

// recherche dans le tableau repSeg des segments qui contiennent le pattern donné
function searchSegRep(pattern) {
  var toks = pattern.split(/ /);
  var motif = "";
  for (var i in toks) {
    var tok = toks[i];
    if (tradCat[tok]) {
      motif += " [^ _]+_" + tradCat[tok];
    } else {
      motif += " " + tok + "_[^_ ]+";
    }
  }
  motif = motif.replace(/^ /, "");
  var regexp = new RegExp(motif);

  // remplissage du tableau contenant les expressions filtrées
  var tabResult = [];
  var longMin = $("#longMinSegRep").val();
  var freqMin = $("#freqMinSegRep").val();
  for (var i = longMin; i <= 7; i++) {
    for (var seg in repSeg[i]) {
      if (seg.match(regexp) && !seg.match(/@card@|_NAM|_PUN/) && repSeg[i][seg] >= freqMin) {
        var s = seg.replace(/_[^_ ]+/g, "");
        tabResult.push([i, s, repSeg[i][seg]]);
      }
    }
  }

  // d'abord suppression si besoin est
  if (segRepTable) {
    $('#segRepTable').dataTable().fnDestroy();
    $('#segRepTable').remove();
    $('#segRepTableDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="segRepTable"></table>');
  }
  // construction du tableau avec dataTable
  $('#segRepTable').dataTable({
    dom: 'T<"clear">lfrtip',
    "aaData": tabResult,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    "sPaginationType": "four_button",
    "aaSorting": [
      [0, "desc"]
    ],
    "aoColumns": [{
        "sTitle": "Longueur"
      },
      {
        "sTitle": "Segment",
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sTitle": "Fréquence",
        "sClass": "center"
      }
    ],
  });
  segRepTable = true;


}


// affichage de l'histogramme  pour les tranches de fréquence
function DisplayHisto() {
  var chart;

  for (var i = 1; i <= 6; i++) {
    if (!tranches[i]) {
      tranches[i] = 0;
    }
  }

  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'histo_freq',
      defaultSeriesType: 'bar',
      borderWidth: 2,

    },
    title: {
      text: 'Tranches de fréquences'
    },
    subtitle: {
      text: 'Histogramme'
    },
    xAxis: {
      categories: ["absent du corpusRef", "1-999", "1000-2999", "3000-6999", "7000-14999", "15000-30999", "31000-63000"],
    },
    yAxis: {
      alternateGridColor: '#FDFFD5',
      minorTickInterval: 'auto',
      minorTickLength: 0,
      title: {
        text: 'Répartition des vocables',
      },
      labels: {
        formatter: function() {
          return this.value
        }
      }
    },
    tooltip: {
      crosshairs: true,
      shared: true
    },
    plotOptions: {
      bar: {}
    },
    series: [{
      name: 'Répartition du vocabulaire par tranches de fréquence',
      marker: {
        symbol: 'cercle'
      },
      data: tranches
    }]
  });

}

function displayVocGrowth() {
  $('#acc_voc').highcharts({
    title: {
      text: 'Accroissement du vocabulaire (lemmes)'
    },
    subtitle: {
      text: 'Par paliers de 100 occurrences'
    },
    yAxis: {
      title: {
        text: 'Taille du vocabulaire'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#808080'
      }],
      floor: 0,
    },
    xAxis: {
      title: {
        text: "Nombre d'occurrences (divisé par 100)"
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },
    series: [{
      name: 'Lemmes différents',
      data: accVoc
    }]
  });
}

function displayDatablesWithNoPagination() {
  // création des tables dataTables
  $('#specifiques').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      }
    ],
  });
  $('#tranches').dataTable({
    "bDestroy": true
  });
  $('#acc_voc_tab').dataTable({
    "bDestroy": true
  });
  $('#parties_du_discours').dataTable({
    "bDestroy": true
  });

  $('#morpho_v').dataTable({
    "bDestroy": true
  });
  $('#lemma_ver').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ],
  });
  $('#lemma_nom').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ],
  });
  $('#lemma_adj').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ]
  });

  $('#lemma_adv').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ]
  });
  $('#lemma_all').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      }
    ]
  });
  $('#form_all').dataTable({
    "bDestroy": true,
    "bPaginate": false,
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordNoLemma clickable"
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      }
    ]
  });
}

function displayDatables() {
  // création des tables dataTables
  $('#specifiques').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bDestroy": true,
    "sPaginationType": "four_button",
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      }
    ]
  });
  $('#tranches').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bPaginate": false,
    "bDestroy": true
  });
  $('#acc_voc_tab').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //~ "sPaginationType": "full_numbers",
    "sPaginationType": "four_button",

    "bDestroy": true
  });
  $('#morpho_v').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bPaginate": false,
    "bDestroy": true
  });
  $('#lemma_ver').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bDestroy": true,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //~ "sPaginationType": "full_numbers",
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ],
  });
  $('#lemma_nom').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bDestroy": true,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //"sPaginationType": "full_numbers",
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ],
  });
  $('#lemma_adj').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bDestroy": true,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //"sPaginationType": "full_numbers",
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ]
  });

  $('#lemma_adv').dataTable({
    dom: 'T<"clear">Blfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "bDestroy": true,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //"sPaginationType": "full_numbers",
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      }
    ]
  });
  $('#lemma_all').dataTable({
    dom: 'T<"clear">Blfrtip',
    "bDestroy": true,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //"sPaginationType": "full_numbers",
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordLemma clickable"
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      }
    ]
  });
  $('#form_all').dataTable({
    dom: 'T<"clear">Blfrtip',
    "bDestroy": true,
    "aLengthMenu": [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "Tous"]
    ],
    //"sPaginationType": "full_numbers",
    "sPaginationType": "four_button",
    "aoColumns": [{
        "sClass": ""
      },
      {
        "sClass": "searchConcordNoLemma clickable"
      },
      {
        "sClass": ""
      },
      {
        "sClass": ""
      }
    ]
  });
}

$(document).ready(function() {
  $('#save').on("click", function() {
    $(".dataTables_wrapper > .dataTables_wrapper").unwrap();
    displayDatablesWithNoPagination();
    $('.dataTables_length, .dataTables_info, .dataTables_paginate, .dataTables_filter').remove();
    alert("Vous pouvez maintenant effectuer une sauvegarde en tapant Ctrl+S (avant de fermer ce dialogue)");
    displayDatables();
  });

  displayDatables();

  // affichage de l'histogramme
  DisplayHisto();
  // affichage de la courbe d'accroissement du vocabulaire
  displayVocGrowth();


  searchSegRep(".*");
  searchPattern("DET NOM");

  // dimensionnement vertical dynamique
  // attention en mode quirks, prendre document.body.clientHeight

  $('#corps').css("height", document.documentElement.clientHeight - 16 + "px"); // 16px de margin au dessus et au dessous de #corps
  $('#statistiques').css("height", (document.documentElement.clientHeight - 40 - 40 - 16) + "px"); // 40px -> pied +40px padding de statistiques


  $(window).resize(function() {
    $('#corps').css("height", document.documentElement.clientHeight - 16 + "px");
    $('#statistiques').css("height", (document.documentElement.clientHeight - 40 - 40 - 16) + "px");
  });


  // évènement click sur les colonnes de datatables
  $('body').on("click", '#coocTable .clickable', function() {
    var fenLeft = $("#fenLeft").val();
    var fenRight = $("#fenRight").val();
    var pattern = $("#pivot2").val();
    var cooc = $(this).html();
    var newExp = pattern + " TOK{0," + fenRight + "} (" + cooc + ")|(" + cooc + ") TOK{0," + fenLeft + "} " + pattern;
    $("#pivot").val(newExp);
    $("#useLemmaConcord")[0].checked = true;
    searchConcord(newExp);
    $('a[href="#concord"]')[0].click();
  });

  $('body').on("click", ".searchConcordNoLemma", function() {
    var newExp = "(" + $(this).html() + ")";
    $("#useLemmaConcord")[0].checked = false;
    $("#pivot").val(newExp);
    searchConcord(newExp);
    $('a[href="#concord"]')[0].click();
  });

  $('body').on("click", ".searchConcordLemma", function() {
    var newExp = "(" + $(this).html() + ")";
    $("#pivot").val(newExp);
    $("#useLemmaConcord")[0].checked = true;
    searchConcord(newExp);
    $('a[href="#concord"]')[0].click();
  });

  //affichage de la div #lecture
  $('body').on("click", ".displayPage", function() {
    var percent = $(this).html();
    percent = percent.substr(0, percent.length - 1);
    pageNum = Math.floor(txt.length * (percent / 100) / pageLength) + 1;
    displayPage();
  });

  // fermeture de la div #lecture
  $('html').click(function(event) {
    if (!event.target.id.match(/page|lecture|back|forward/) && !event.target.className.match(/displayPage/)) {
      $("#lecture").fadeOut();
    }
  });

  // gestion des flèches lors de l'affichage des pages
  $('body').on("keypress", function(event) {
    if ($("#lecture").css("display") == "block") {
      if (event.keyCode == 39) {
        pageForward();
      } else if (event.keyCode == 37) {
        pageBack();
      }
    }
  });
  // gestion des boutons back et forward
  $('body').on("click", "#back", pageBack);
  $("#back").on("mousedown", function() {
    $("#back").attr("src", "../../images/back_enabled_hover.png");
  });
  $("#back").on("mouseup", function() {
    $("#back").attr("src", "../../images/back_enabled.png");
  });
  $("#back").on("mouseout", function() {
    $("#back").attr("src", "../../images/back_enabled.png");
  });

  $('body').on("click", "#forward", pageForward);
  $("#forward").on("mousedown", function() {
    $("#forward").attr("src", "../../images/forward_enabled_hover.png");
  });
  $("#forward").on("mouseout", function() {
    $("#forward").attr("src", "../../images/forward_enabled.png");
  });
  $("#forward").on("mouseup", function() {
    $("#forward").attr("src", "../../images/forward_enabled.png");
  });


});


$.fn.dataTableExt.oPagination.four_button = {
  /*
   * Function: oPagination.four_button.fnInit
   * Purpose:  Initalise dom elements required for pagination with a list of the pages
   * Returns:  -
   * Inputs:   object:oSettings - dataTables settings object
   *           node:nPaging - the DIV which contains this pagination control
   *           function:fnCallbackDraw - draw function which must be called on update
   */
  "fnInit": function(oSettings, nPaging, fnCallbackDraw) {
    nFirst = document.createElement('span');
    nPrevious = document.createElement('span');
    nNext = document.createElement('span');
    nLast = document.createElement('span');

    nFirst.appendChild(document.createTextNode(oSettings.oLanguage.oPaginate.sFirst));
    nPrevious.appendChild(document.createTextNode(oSettings.oLanguage.oPaginate.sPrevious));
    nNext.appendChild(document.createTextNode(oSettings.oLanguage.oPaginate.sNext));
    nLast.appendChild(document.createTextNode(oSettings.oLanguage.oPaginate.sLast));

    nFirst.className = "paginate_button first";
    nPrevious.className = "paginate_button previous";
    nNext.className = "paginate_button next";
    nLast.className = "paginate_button last";

    nPaging.appendChild(nFirst);
    nPaging.appendChild(nPrevious);
    nPaging.appendChild(nNext);
    nPaging.appendChild(nLast);

    $(nFirst).click(function() {
      oSettings.oApi._fnPageChange(oSettings, "first");
      fnCallbackDraw(oSettings);
    });

    $(nPrevious).click(function() {
      oSettings.oApi._fnPageChange(oSettings, "previous");
      fnCallbackDraw(oSettings);
    });

    $(nNext).click(function() {
      oSettings.oApi._fnPageChange(oSettings, "next");
      fnCallbackDraw(oSettings);
    });

    $(nLast).click(function() {
      oSettings.oApi._fnPageChange(oSettings, "last");
      fnCallbackDraw(oSettings);
    });

    /* Disallow text selection */
    $(nFirst).bind('selectstart', function() {
      return false;
    });
    $(nPrevious).bind('selectstart', function() {
      return false;
    });
    $(nNext).bind('selectstart', function() {
      return false;
    });
    $(nLast).bind('selectstart', function() {
      return false;
    });
  },

  /*
   * Function: oPagination.four_button.fnUpdate
   * Purpose:  Update the list of page buttons shows
   * Returns:  -
   * Inputs:   object:oSettings - dataTables settings object
   *           function:fnCallbackDraw - draw function which must be called on update
   */
  "fnUpdate": function(oSettings, fnCallbackDraw) {
    if (!oSettings.aanFeatures.p) {
      return;
    }

    /* Loop over each instance of the pager */
    var an = oSettings.aanFeatures.p;
    for (var i = 0, iLen = an.length; i < iLen; i++) {
      var buttons = an[i].getElementsByTagName('span');
      if (oSettings._iDisplayStart === 0) {
        buttons[0].className = "paginate_disabled_previous";
        buttons[1].className = "paginate_disabled_previous";
      } else {
        buttons[0].className = "paginate_enabled_previous";
        buttons[1].className = "paginate_enabled_previous";
      }

      if (oSettings.fnDisplayEnd() == oSettings.fnRecordsDisplay()) {
        buttons[2].className = "paginate_disabled_next";
        buttons[3].className = "paginate_disabled_next";
      } else {
        buttons[2].className = "paginate_enabled_next";
        buttons[3].className = "paginate_enabled_next";
      }
    }
  }
};

// go to the previous page and display it
function pageBack() {
  if (pageNum > 1) {
    pageNum--;
  }
  displayPage();
}

// go to the next page and display it
function pageForward() {
  if (pageNum < pageTot) {
    pageNum++;
  }
  displayPage();
}

// display the current page
function displayPage() {
  // calcul de initIndex et enIndex en fonction de percent

  var initIndex = (pageNum - 1) * pageLength;
  if (initIndex <= 0) {
    initIndex = 0;
  }
  // calage sur l'espace qui précède
  while (txt[initIndex] != " " && initIndex > 0) {
    initIndex--;
  }

  var endIndex = initIndex + pageLength;
  if (endIndex >= txt.length - 1) {
    endIndex = txt.length - 1;
  } else {
    // calage sur l'espace qui précède
    while (txt[endIndex] != " ") {
      endIndex--;
    }
  }

  var page = txt.substr(initIndex, endIndex - initIndex);
  $("#page").html(page);
  $("#pageNum").html(pageNum);
  $("#pageTot").html(pageTot);
  $("#lecture").fadeIn();
}
