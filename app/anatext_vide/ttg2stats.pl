########################## Perl Modules
use IO::Handle;
use DB_File;
use Encode;
use strict;
use Getopt::Long;
use utf8;
binmode(STDOUT,":utf8");

STDOUT->autoflush();

my $verbose=0;

#************************************************************************************************** var. globales

########################## Arguments

# 1 seul arguments : le nom du fichier
our $fileIn = $ARGV[0];
my $lang="fr";
my $trad="trad";
my $outputFormat="html";

# lecture des arguments
GetOptions (
	'inputFile=s' => \$fileIn,
	'language=s' => \$lang,
	'tagset=s' => \$trad,
	'outputFormat=s' => \$outputFormat,
);

my %ln=(
	"french-utf8"=>'fr',
	"german-utf8"=>'de',
	"portuguese"=>'pt',
	"spanish-utf8"=>'es',
	"italian-utf8"=>'it',
	"english"=>'en',
	"russian-utf8"=>'ru',
	"latin"=>'la',
	"arabic-utf8"=>'ar',
	"ancient-greek-utf8"=>'grc'
);
my $ln="fr";
my $lemmatizedSegRep=1;
my $maxSegRepLength=7;

if (exists($ln{$lang})) {
	$ln=$ln{$lang};
}
our $title;
our $installPath="/var/www/html/anaText/pl";
our $db_lemme_tranche="$installPath/lemme_tranche.$ln.db";	# nom de la base contenant les tranches de chaque lemme
our $db_lemme_freq="$installPath/lemme_freq.$ln.db";  		# nom de la base contenant les fréquences des lemmes
our $db_forme_nbSyll="$installPath/forme_nbSyll.$ln.db";  	# nom de la base contenant les fréquences des lemmes
our $nbCarPerSyll=3.2125;					# nombre moyen de car. par syllabes en fran�ais
our $repeatedSegCriteria="formCat"; # (form|lemma|lemmaCat|formCat)
our $sep1="_";
our $sep2=" ";

my %tradCat=qw(
NAM	npr
NUM	num
DTJJ	adj
DTNN	nom
RP	adv
NNP	npr
JJ	adj
JJR	adj
JJS	adj
RB	adv
RBR	adv
RBS	adv
WRB	adv
FW	aut
LS	aut
CC	con
IN	con
DT	det
PDT	det
WDT	det
UH	int
NN	nom
NP	npr
NPS	npr
IN	pre
TO	pre
PP	pro
EX	pro
PP$	pro
WP	pro
WP$	pro
SYM	sym
MD	ver
VB	ver
VBD	ver
VBG	ver
VBN	ver
VBP	ver
VBZ	ver
NE	nom
VVFIN	ver
VAFIN	ver
VVPP	ver
ADJA	adj
ADJD	adj
ART	det
APPR	pre
KOUS	con
PPER	pro
PTKZU	pre
N	nom
V	ver
PRON	pro
ADJ	adj
ADV	adv
ESSE	ver
PREP	pre
CS	con
PRP	pre
POSS	adj
l	art
n	nom
a	adj
p	pro
v	ver
d	adv
r	app
c	con
m	num
i	int
g	part
u	pon
b	aut
k	aut
);

my %tradMsd=qw(
CDi	cardi
JJR	compa
RBR	compa
CCi	coord
LS	ilist
VB	ipres
MD	modal
CD	num
RP	parti
VBZ	pers3
PP	perso
NNS	pluri
POS	posse
PP$	posse
WP$	posse
VBN	ppass
VBD	prete
WBG	progr
VBG	progr
WP	relat
NN	singu
JJS	super
RBS	super
);



########################## globales

our $ligne=0;
#tableau associatif dans lequel est enregistr� le nombre de syllabes par mot.
our %type2freq;			# fréquence par type pour les formes
our %typeLem2freq;		# fréquence par type pour les lemmes
our %typeLemNom2freq;	# fréquence par type pour les lemmes noms
our %typeLemVer2freq;	# fréquence par type pour les lemmes verbes
our %typeLemAdj2freq;	# fréquence par type pour les lemmes adj
our %typeLemAdv2freq;	# fréquence par type pour les lemmes adv

our %lemme_tranche; # cle: un lemme, valeur: sa tranche de fréquence (1000 premiers, 2000 suivants, 4000 suivants etc)
our %lemme_freq;    # cle: un lemme, valeur: sa fréquence relative en nb d'occ. par million de mots (Frantext)
our %forme_nbSyll;	# cle : un lemme, valeur: le nombre de syllabe correspondant
# cr�ation du lien entre bd et %

tie (%lemme_tranche,"DB_File",$db_lemme_tranche);
tie (%lemme_freq,"DB_File",$db_lemme_freq);
tie (%forme_nbSyll,"DB_File",$db_forme_nbSyll);

my $refCorpusSize=exists($lemme_freq{"<corpuSize>"})? $lemme_freq{"<corpuSize>"} : 0 ;
my $specName= exists($lemme_freq{"<corpuSize>"})? "LogLike":"Ratio de fréquences";
#~ do {
	#~ my $car=0;
	#~ $sylls=0;
	#~ open(IN,"form_nbsyll.txt");
	#~ while (<IN>) {
		#~ ($form,$nbSyll)=split(/\t/);
		#~ $lemme_nbSyll{$form}=$nbSyll;
		#~ $cars+=length($form);
		#~ $sylls+=$nbSyll;
	#~ }
	#~ print "Nombre moyen de car. par syllabes : ".($cars/$sylls)."\n";
	#~ die;
#~ } unless 0;

#************************************************************************************************** fonctions

# renvoie le nombre de syllabes pour une forme donn�e
sub nbSyll {
	my $forme=shift;
	my $calc=int(length($forme)/$nbCarPerSyll+0.5);

	Encode::_utf8_off($forme);
	#pour ne pas prendre en compte les ponctuations et les num�riques
	if (exists($forme_nbSyll{$forme})) {
		return $forme_nbSyll{$forme};
	} else  {
		return $calc;
	}
}

#================================================= ttg2txs($fileTTG)

# Transformation d'un texte en sortie de TreeTagger, vers le format txs, en ajoutant des tag0 issus de dico
sub ttg2stats {
	my $now=time();
	my $fileIn=shift;

    #d�claration des diff�rentes variables n�cessaire pour r�cup�rer nos donn�es
    # taille du vocabulaire, compt� en lemme
    my $vocSize=0;
    my %voc; # hachage enregistrant chaque lemme une fois
	my @accVoc; # liste de couples [nbOcc,nbVoc] enregistrant l'accroissement du vocabulaire

    #nombre phrases
	my $nbSent=1;
    #nombre de tokens
	my $nbTok=0;
    #nombre de formes
	my $nbForm=0;
    #nombre de caract�res dans les formes
	my $nbCar=0;
    #nombre de mots inconnus de treetagger
	my $nbUnknown=0;
    #nombre de lemmes inconnus du dictionnaire des fréquences
	my $nbLemmesInconnus=0;
    #tableau dans lequel on stocke le nombre de lemmes pour chaque tranche
    my @tranche_nb;
    #tableau des fréquences des lemmes (pour VER, NOM, ADJ)
    my %freq;
    #tableau des specificites des lemmes
    my %specificite;
	# liste enregistrant 6 hachages pour les segments répétés de longueur 2..7 - Forme + Cat
	our @repeatedSegs=({},{},{},{},{},{});
	# d�claration du hachage enregistrant les segments répétés trouv�s dans le fichier
	my %repeatedSegs=();
	# liste enregistrant 6 hachages pour les segments répétés de longueur 2..7 - Lemme + Cat
	our @repeatedSegs=({},{},{},{},{},{});
	# d�claration du hachage enregistrant les segments répétés trouv�s dans le fichier
	# d�claration de la liste enregistrant les 7 derni�res formes lus dans la phrase
	my @window=();
	# d�claration de la liste enregistrant les 7 derni�res formes lus dans la phrase
	my @last7lems=();
    #tableau des lemmes tri�s par ordre de specificite décroissante
    my @keywords;
    #nombre de verbes � l'infintif
	my $nbinf=0;
    #nombre de verbes au pr�sent de l'indicatif
	my $nbindpre=0;
    #nombre de verbes � l'imparfait de l'indicatif
	my $nbindimp=0;
    #nombre de verbes au pass� simple de l'indicatif
	my $nbindpsi=0;
    #nombre de verbes au futur de l'indicatif
	my $nbindfut=0;
    #nombre de verbes au pr�sent du subjonctif
	my $nbsubpre=0;
    #nombre de verbes au pass� du subjonctif
	my $nbsubpas=0;
    #nombre de verbes au conditionnel pr�sent
	my $nbconpre=0;
    #nombre de verbes au conditionnel pass�
	my $nbconpas=0;
    #nombre de participes pr�sent
	my $nbppr=0;
    #nombre de participes pass�
	my $nbppa=0;
    #nombre de n�gations
    my $nbneg=0;
    #nombre de conjonctions de subordination
	my $nbconj=0;
    #nombre de pronoms relatifs
    my $nbrel=0;
    #nombre de syllabes dans le texte
    my $nbSyll=0;
    #nombre de type dans le texte
    my $nbType=0;
    #nombre de type par lemme dans le texte
    my $nbTypeLemme=0;
    #forme du mot, sa cat�gorie, son lemme et ses traits
	my ($forme,$cat,$lemme,$msd);

	my %statsMsd;
	my %statsPos;
	# On �tablit des liens entre des segments répétés et les segments incluant d'ordre imm�diatement sup�rieur (contenant une forme de plus)
	# ce tableau sert au d�doublonnage : tout segment r�p�t� ayant le m�me nombre d'occurrences que les segments imm�diatement incluant seront supprimés
	my %includingSegs;

	my $texte="";
	#~ my $texteLem="";

	local *IN;

	open (IN,"<:encoding(utf8)",$fileIn);
	open (TTGXML,">:encoding(utf8)",$fileIn.".xml");
	open (OUT,">:encoding(utf8)",$fileIn.".html");
	print TTGXML qq |<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet href="/anaText/ttg2table.xsl" type="text/xsl"?>
<ttg>
|;

	# boucle principale
	while (<IN>) {
		$ligne++;
		if (/(.*)\t([A-Z\-]+)(:([\w:]+))?\t(.*)\n/i) {
			$forme=$1;
			$cat=$2;
			$msd=$4;
			$verbose && print "forme $forme cat $cat\n";
			if ($trad eq "trad") {
				if (exists($tradCat{$cat})) {
					$cat=uc($tradCat{$cat});
				# cas o� on ne prend que la premi�re lettre, cf, grec
				} elsif (exists($tradCat{substr($cat,0,1)})) {
					$cat=uc($tradCat{substr($cat,0,1)});
				}
				if (exists($tradMsd{$cat})) {
					$msd=$tradMsd{$cat};
				}
			}
			$lemme=$5;
			print TTGXML "<t><n>$ligne</n><w>$forme</w><c>$cat</c><f>$msd</f><l>$lemme</l></t>\n";
			$statsPos{$cat}++;

			#d�finition des traits
			if (!$msd) {
				$msd="";
			} else {
				# mise � jour des stats par traits
				if (! exists($statsMsd{$cat})) {
					$statsMsd{$cat}={};
				}
				if (! exists($statsMsd{$cat}{$msd})) {
					$statsMsd{$cat}{$msd}=0;
				}
				$statsMsd{$cat}{$msd}++;

			}

			$nbTok++;

			#d�finition des phrases
            if ($cat ne "SENT" && $cat ne "PUN" && $cat ne "NUM"){
				$nbForm++;
				if ($nbForm % 100==0) {
					push(@accVoc,$vocSize);
				}
				if (!exists($voc{$lemme})) {
					$vocSize++;
					$voc{$lemme}=1;
				}

				$nbCar+=length($forme);

				# calcul du nombre de syllabes
				# on ne prendre pas en compte les ponctuations et les num�riques
				$nbSyll+=nbSyll($forme);

				# mise � jour du nombre de lemmes par tranche de fréquence
				if ($cat ne "NPR") {
					my $key=$lemme;
					Encode::_utf8_off($key);
					if (exists($lemme_tranche{$key})) {
						$verbose && print "Key : $key -> ".$lemme_tranche{$key}."\n";
						$tranche_nb[$lemme_tranche{$key}]++;
					} else {
						$nbLemmesInconnus++;
					}
				}
			}



            # construction du tableaux des fréquences pour extraire les mots cl�s (enregistre les fréquences de chaque lemme)
            if ($cat =~/^(NOM|VER|ADJ)$/i && $lemme!~/^(�tre|avoir)$/) {
                if (!exists($freq{$lemme})) {
                    $freq{$lemme}=0;
                }
                $freq{$lemme}++;
            }

			# compteur des types
			if  ($cat ne "NUM") {
				if (exists($type2freq{$forme})){
					$type2freq{$forme}++;
				} else{
					$type2freq{$forme}=1;
					$nbType++;
				}
				if(exists($typeLem2freq{$lemme})){
					$typeLem2freq{$lemme}++;
				} else{
					$typeLem2freq{$lemme}=1;
					$nbTypeLemme++;
				}
			}

			if	($cat eq "NOM") {
				if (exists($typeLemNom2freq{$lemme})){
					$typeLemNom2freq{$lemme}++;
				} else{
					$typeLemNom2freq{$lemme}=1;
				}
			} elsif ($cat eq "VER") {
				if (exists($typeLemVer2freq{$lemme})){
					$typeLemVer2freq{$lemme}++;
				} else{
					$typeLemVer2freq{$lemme}=1;
				}
			} elsif ($cat eq "ADJ") {
				if (exists($typeLemAdj2freq{$lemme})){
					$typeLemAdj2freq{$lemme}++;
				} else{
					$typeLemAdj2freq{$lemme}=1;
				}
			} elsif ($cat eq "ADV") {
				if (exists($typeLemAdv2freq{$lemme})){
					$typeLemAdv2freq{$lemme}++;
				} else{
					$typeLemAdv2freq{$lemme}=1;
				}
			}

			#compteur mot inconnu
			if ($lemme =~/unknown/i){
				$nbUnknown++;
			}

			if ($cat eq "VER"){

				# verbe � l'infinitif
				SWiTCH: {
					if ($msd eq "infi"){
						$nbinf++;
						last;
					}

					# verbe � l'indicatif pr�sent
					if ($msd eq "pres"){
						$nbindpre++;
						last;
					}

					# verbe � l'indicatif imparfait
					if ($msd eq "impf"){
						$nbindimp++;
						last;
					}

					# verbe � l'indicatif pass� simple
					if ($msd eq "simp"){
						$nbindpsi++;
						last;
					}

					# verbe � l'indicatif futur
					if ($msd eq "futu"){
						$nbindfut++;
						last;
					}

					# verbe au subj pr�sent
					if ($msd eq "subp"){
						$nbsubpre++;
						last;
					}

					# verbe au subj imparfait
					if ($msd eq "subi"){
						$nbsubpas++;
						last;
					}

					# verbe au conditionnel pr�sent
					if ($msd eq "cond"){
						$nbconpre++;
						last;
					}

					# verbe au participe pr�sent
					if ($msd eq "ppre"){
						$nbppr++;
						last;
					}

					# verbe au participe pr�sent
					if ($msd eq "pper"){
						$nbppa++;
						last;
					}
				}

			} # fin de if(cat eq VER)

            #~ #d�compte des n�gations
            #~ if ($lemme eq "ne"){
                #~ $nbneg++;
            #~ }

            #~ #d�compte des conjonctions
			#~ if (($cat eq "KON") &&($lemme ne ("mais"|"ou"|"et"|"donc"|"or"|"ni"|"car"))){ #  !!!!!!!! NE MARCHE PAS : UTILISEZ UN MATCHING
            #~ if ($cat eq "KON" && $lemme !~/^(mais|ou|et|donc|or|ni|car)$/i) {
                #~ $nbconj++;
            #~ }
			#~ #d�compte des relatives
            #~ if (($cat eq "PRO") &&($msd eq "REL")){
                #~ $nbrel++;
            #~ }

			if ($cat eq "SENT") {
				$nbSent++;
			}

			# traitement des segments répétés FORM + CAT ou LEMMA + CAT
			# on empile la forme dans la liste @window

			if ($lemmatizedSegRep) {
				push(@window,$lemme.$sep1.$cat);
			} else {
				push(@window,$forme.$sep1.$cat);
			}

			# si la liste contient plus que 7 formes on d�cale
			if (@window>$maxSegRepLength) {
				shift(@window);
			}


			# on met � jour les segments répétés pour chaque longueur entre 7 et 2
			my $lastSeg;
			# on parcourt les segments répétés en r�duisant � gauche
			for (my $i=0;$i<$#window;$i++) {
				my $segment=join($sep2,@window[$i..$#window]);
				#~ print $segment."\n";
				if (exists($repeatedSegs[$#window-$i]{$segment})) {
					$repeatedSegs[$#window-$i]{$segment}++;
				} else {
					$repeatedSegs[$#window-$i]{$segment}=1;
				}
				# mise � jour des fréquences de document pour les segments répétés globaux
				if (!exists($repeatedSegs{$segment})) {
					$repeatedSegs{$segment}=1;
				} else {
					$repeatedSegs{$segment}++;
				}
				# mise � jour du lien entre le segment et le segment incluant, qui comporte une forme de plus � gauche (lastSeg)
				if ($lastSeg) {
					$includingSegs{$segment}{$lastSeg}=1;
				}
				$lastSeg=$segment;
			}

			# on parcourt les segments répétés dans l'autre sens (on r�duit � droite) afin d'�tablir les liens d'inclusions vers les segments comportant une forme de plus � droite
			$lastSeg=join($sep2,@window[0..$#window]);
			for (my $i=1;$i<$#window;$i++) {
				my $segment=join($sep2,@window[0..($#window-$i)]);
				$includingSegs{$segment}{$lastSeg}=1;
				$lastSeg=$segment;
			}
			# Enregistrement du texte
			$texte.=" ".$forme.$sep1.$lemme.$sep1.$cat;
			#~ $texteLem.=" ".$lemme.$sep1.$cat;

		} else {
			# warn "Anomalie apr�s la phrase s$nbSent\n";
		}
	}
	# fin de la boucle de lecture

	print TTGXML "</ttg>\n";
	close(TTGXML);


	$verbose && print "écriture js/".$fileIn.".txt.js\n";
	open(JS,">:encoding(utf8)","js/".$fileIn.".txt.js");
	$texte=~s/\\//g;
	$texte=~s/"/\\"/g;
	print JS "var text=\"$texte\";\n";
	close(JS);

	#~ open(JS,">:encoding(utf8)",$fileIn.".txt.lem.js");
	#~ $texte=~s/\\//g;
	#~ $texteLem=~s/"/\\"/g;
	#~ print JS "var textLem=\"$texteLem\";\n";
	#~ close(JS);

	$verbose && print "écriture de js/".$fileIn.".tab.js\n";
	open(JS2,">:encoding(utf8)","js/".$fileIn.".tab.js");
	print JS2 "var repSeg=[];\n";
	# on imprime les segments répétés pour chaque longueur entre 7 et 2
	for (my $i=1;$i<=$#repeatedSegs;$i++) {
		print JS2 "repSeg[".($i+1)."]=[];\n";
		foreach my $segment (sort {$repeatedSegs[$i]{$b} <=> $repeatedSegs[$i]{$a}} keys %{$repeatedSegs[$i]}) {
			# d�doublonnage
			if (exists($includingSegs{$segment})) {
				foreach my  $includingSegs (keys %{$includingSegs{$segment}}) {
					if ($repeatedSegs[$i+1]{$includingSegs} == $repeatedSegs[$i]{$segment}) {
						$repeatedSegs[$i]{$segment}=0; # on annule les occurrences de segment, qui sera repr�sent� dans son incluant
					}
				}
			}
			if ($repeatedSegs[$i]{$segment}>1) {
				my $escaped=$segment;
				$escaped=~s/\\//g;
				$escaped=~s/'/\\'/g;
				print JS2 "repSeg[".($i+1)."]['$escaped']=".$repeatedSegs[$i]{$segment}.";\n";
			}
		}
	}
	# on imprime le tableau contenant les fréquences des lemmes (utile pour calculer les mesures d'association)
	print JS2 "var nbOccTot=$nbTok;\n";
	print JS2 "var lemme2freq=[];\n";
	for my $lem (keys %typeLem2freq) {
		my $escaped=$lem;
		$escaped=~s/\\//g;
		$escaped=~s/'/\\'/g;
		print JS2 "lemme2freq['$escaped']=".$typeLem2freq{$lem}.";\n";
	}
	close(JS2);

	#~ $nbSyll=sprintf("%.2f", $nbSyll); # arrondi � la deuxi�me d�cimale
	$nbSyll=int($nbSyll);

	$verbose && print "calcul des lemmes spécifiques\n";

    # calcul des mots spécifiques
    foreach $lemme (keys %freq) {
		my $key=$lemme;
		Encode::_utf8_off($key);
        if (exists($lemme_freq{$key}) && $lemme_freq{$key}>0) {
			# calcul du loglike quand c'est possible
			if ($refCorpusSize>0) {
				$specificite{$lemme}=computeLL($nbTok,$lemme_freq{$key}*($refCorpusSize/1000000),$freq{$lemme},$refCorpusSize);
				print "Specificit� $lemme (LL) : ".$specificite{$lemme}."\n";
			} else {

				my $freqParM=$freq{$lemme}*1000000/$nbForm;
				if ($freqParM>1000) {
					# la specificite est d�finie comme le rapport entre la fréquence dans le texte par la fréquences dans Frantext
					$specificite{$lemme}=$freqParM/$lemme_freq{$key};
					print "Specificit� $lemme: ".$specificite{$lemme}."\n";
				}
			}
		#~ } else {
			#~ if (!exists($lemme_freq{$key})) {
				#~ print "lemme_freq{$key} n'existe pas \n";
			#~ }
		}
    }
    # tri par valeurs décroissantes des cl�s de %specificite
    @keywords= sort {$specificite{$b}<=>$specificite{$a}} grep {abs($specificite{$_})>10.83}  keys %specificite;

	my $tranches=join(",",@tranche_nb);
	my $accVoc=join(",",@accVoc);

	#cr�ation de l'en-t�te du fichier HTML
	open(IN,"<:utf8",$fileIn.".titre");
	$title=<IN>;
	close(IN);
	chomp $title;

	$verbose && print "écriture du HTML\n";
	if ($outputFormat eq "html") {
		print OUT qq |<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		"http://www.w3.org/TR/html4/strict.dtd">
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>Analyse lexicométrique</title>
			<link rel="stylesheet" type="text/css" href="./css/jquery.dataTables.css" />
			<link rel="stylesheet" type="text/css" href="./css/tableTools.css" />
			<link rel="stylesheet" type="text/css" href="./css/anaText.css" />
		</head>
		<body>
		|;

		print OUT "		<div id=\"corps\">\n";

		# écriture du menu
		print OUT qq |
				<div id="menu">
					<div class="titre">AnaText 2.3</div>
					<p class="link"><a href="$fileIn.xml" target="blank">Texte étiqueté (utf-8)</a></p>
					<p class="link"><a href="$fileIn" target="blank">Sortie brute de Treetagger</a></p>
					<ul>
						<li><a href="#statistiques_generales" class="gen">Statistiques générales</a></li>
						<li><a href="#mots_specifiques" class="gen">Lemmes spécifiques</a></li>
						<li><a href="#tranches_de_frequence" class="gen">Tranches de fréquence</a></li>
						<li><a href="#accroissement_vocabulaire" class="gen">Accroissement du voc.</a></li>
						<li><a href="#parties_du_discours"  class="morpho">Parties du discours</a></li>
						<li><a href="#morphologie_verbale"  class="morpho">Morphologie verbale</a></li>
						<li><a href="#noms_lemmatises" class="freq">Noms lemmatisés</a></li>
						<li><a href="#verbes_lemmatises"  class="freq">Verbes lemmatisés</a></li>
						<li><a href="#adjectifs_lemmatises"  class="freq">Adjectifs lemmatisés</a></li>
						<li><a href="#adverbes_lemmatises"  class="freq">Adverbes lemmatisés</a></li>
						<li><a href="#lemmes"  class="freq">Tous les lemmes</a></li>
						<li><a href="#formes"  class="freq">Toutes les formes</a></li>
						<li><a href="#concord"  class="pat">Concordance</a></li>
						<li><a href="#cooc"  class="pat">Cooccurrences</a></li>
						<li><a href="#pattern"  class="pat">Recherche de patterns</a></li>
						<li><a href="#seg_rep"  class="pat">Segments répétés</a></li>
					</ul>
					<br/>
					<center><button id='save'>Sauvegarder</button></center>
				</div>
				<div id="lecture">
					<div id='page'></div>
					<div id='pageN'>Page <span id="pageNum"></span>/<span id="pageTot"></span></div>
					<center><img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/back_enabled.png"  id="back"/> <img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/forward_enabled.png" id="forward"/></center>
					<img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/back_enabled_hover.png"  id="back_hover"/> <img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/forward_enabled_hover.png" id="forward_hover"/>
				</div>

	|;

		#ecriture des statistiques

		print OUT "			<div id=\"statistiques\">\n";
		print OUT "				<p class=\"titre\">Texte analys&eacute; : <span id='title'>$title</span></p>\n";
		print OUT "				<a style=\"color:red;font-size:12px\" onmouseover=\"this.style.cursor='pointer'\" onclick=\"alert('Pour ne pas encombrer le serveur, les résultats de l\\'analyse sont supprimés au bout de 24h : pour les conserver localement, faites \\'enregistrez-sous\\' dans votre navigateur, apr�s avoir cliqu� sur le bouton \\'Sauvegarder\\' en bas du menu\\n');\">Lire l'avertissement</a><br/>";
		print OUT "				<a name=\"statistiques_generales\"></a>\n";
		print OUT "				<h1>Statistiques g&eacute;n&eacute;rales</h1>\n";
		print OUT "				<table>\n";
		print OUT "					<tr><td colspan=\"2\" class=\"tableTitle\">Occurrences</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Phrases </td><td class=\"value\">$nbSent</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Tokens (formes et ponctuation)</td><td class=\"value\">$nbTok</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Formes</td><td class=\"value\">$nbForm</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Syllabes</td><td class=\"value\">$nbSyll</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Caract&egrave;res (hors ponctuation)</td><td class=\"value\">$nbCar</td></tr>\n";

		print OUT "					<tr><td colspan=\"2\" class=\"tableTitle\">Lisibilité</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Nombre moyen de formes par phrases </td><td class=\"value\">".sprintf("%.1f",$nbForm/$nbSent)."</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Nombre moyen de syllabes par forme </td><td class=\"value\">".sprintf("%.1f",$nbSyll/$nbForm)."</td></tr>\n";

		print OUT "					<tr><td colspan=\"2\" class=\"tableTitle\">Types</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Formes</td><td class=\"value\">$nbType</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Lemmes</td><td class=\"value\">$nbTypeLemme</td></tr>\n";
		print OUT "				</table>\n";

		$verbose && print "écriture des formes spécifiques\n";
		# mots specifiques
		print OUT "				<a name=\"mots_specifiques\"></a>\n";
		print OUT "				<h1>Lemmes sp&eacute;cifiques</h1>\n";
		print OUT "				<table  id=\"specifiques\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fr&eacute;quence</th><th>CorpusRef (par million)</th><th>$specName (spécificité)</th></thead>\n";
		print OUT "					<tbody>\n";

		my $count=0;
		foreach $lemme (@keywords) {

			if ($lemme) {
				$count+=1;
				my $key= $lemme;
				Encode::_utf8_off($key);
				print OUT "						<tr><td class=\"label\">$count</td><td class=\"value\">".toXML($lemme)."</td><td class=\"value\">$freq{$lemme}</td><td class=\"value\">$lemme_freq{$key}</td><td class=\"value\">". sprintf("%.3f", $specificite{$lemme})."</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";


		$verbose && print "écriture des tranches de fréquence\n";
		#tranches de fréquence
		print OUT "				<a name=\"tranches_de_frequence\"></a>\n";
		print OUT "				<h1>Tranches de fréquence</h1>\n";
		print OUT "				<table id=\"tranches\" class=\"display\">\n";
		print OUT "					<thead><th>Tranche</th><th>Nombre d'occurrences</th></thead>\n";
		print OUT "					<tbody>\n";
		# d�finition de l'intervalle de fréquence
		my $inf=0;
		my $sup=1000;
		my $newSup;
		for (my $i=0;$i<=$#tranche_nb;$i++) {
			if (exists( $tranche_nb[$i])) {
				print OUT "						<tr><td class=\"label\">Tranche ".($i+1)."<br/>$inf &lt;= Rang &lt; $sup</td><td class=\"value\">$tranche_nb[$i]</td></tr>\n";
			}

			$newSup=$sup+2*($sup-$inf);
			$inf=$sup;
			$sup=$newSup;
		}
		print OUT "						<td class=\"label\">Absents du corpus de référence</td><td class=\"value\">$nbLemmesInconnus</td>\n";
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";
		print OUT "				<div id=\"histo_freq\"></div>\n";

		$verbose && print "écriture de l'accroissement du vocabulaire\n";
		#accroissement du voc
		print OUT "				<a name=\"accroissement_vocabulaire\"></a>\n";
		print OUT "				<h1>Accroissement du vocabulaire</h1>\n";
		print OUT "				<table id=\"acc_voc_tab\" class=\"display\">\n";
		print OUT "					<thead><th>Nombre d'occurrences</th><th>Nombre de lemmes diff�rents</th></thead>\n";
		print OUT "					<tbody>\n";
		# d�finition de l'intervalle de fréquence
		for (my $i=0;$i<=$#accVoc;$i++) {
			print OUT "						<tr><td class=\"label\">".(($i+1)*100)."</td><td class=\"value\">$accVoc[$i]</td></tr>\n";
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";
		print OUT "				<div id=\"acc_voc\"></div>\n";


		$verbose && print "écriture de la morphologie\n";
		# morphosyntaxe
		print OUT "				<a name=\"parties_du_discours\"></a>\n";
		print OUT "				<h1>Parties du discours</h1>\n";
		print OUT "				<table  id=\"parties_du_discours\" class=\"display\">\n";
		print OUT "					<thead><tr><th>Etiquette</th><th>Nombre d'occurrences</th></tr></thead>\n";
		print OUT "					<tbody>\n";
		foreach my $pos (sort keys %statsPos) {
			print OUT "						<tr><td class=\"label\">$pos</td><td class=\"value\">".$statsPos{$pos}."</td></tr>\n";
		}

		#~ print OUT "						<tr><td class=\"label\">Infinitif</td><td class=\"value\">$nbinf</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif pr&eacute;sent</td><td class=\"value\">$nbindpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif imparfait</td><td class=\"value\">$nbindimp</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif pass&eacute; simple</td><td class=\"value\">$nbindpsi</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif futur</td><td class=\"value\">$nbindfut</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Subjonctif pr&eacute;sent</td><td class=\"value\">$nbsubpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Subjonctif pass&eacute;</td><td class=\"value\">$nbsubpas</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Conditionnel pr&eacute;sent</td><td class=\"value\">$nbconpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Participe pr&eacute;sent</td><td class=\"value\">$nbppr</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Participe pass&eacute;</td><td class=\"value\">$nbppa</td></tr>\n";
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		print OUT "				<a name=\"morphologie_verbale\"></a>\n";
		print OUT "				<h1>Morphologie verbale</h1>\n";
		print OUT "				<table  id=\"morpho_v\" class=\"display\">\n";
		print OUT "					<thead><tr><th>Temps / mode</th><th>Nombre d'occurrences</th></tr></thead>\n";
		print OUT "					<tbody>\n";
		foreach my $msd (sort keys %{$statsMsd{'VER'}}) {
			print OUT "						<tr><td class=\"label\">$msd</td><td class=\"value\">".$statsMsd{'VER'}{$msd}."</td></tr>\n";
		}

		#~ print OUT "						<tr><td class=\"label\">Infinitif</td><td class=\"value\">$nbinf</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif pr&eacute;sent</td><td class=\"value\">$nbindpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif imparfait</td><td class=\"value\">$nbindimp</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif pass&eacute; simple</td><td class=\"value\">$nbindpsi</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif futur</td><td class=\"value\">$nbindfut</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Subjonctif pr&eacute;sent</td><td class=\"value\">$nbsubpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Subjonctif pass&eacute;</td><td class=\"value\">$nbsubpas</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Conditionnel pr&eacute;sent</td><td class=\"value\">$nbconpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Participe pr&eacute;sent</td><td class=\"value\">$nbppr</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Participe pass&eacute;</td><td class=\"value\">$nbppa</td></tr>\n";
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		$verbose && print "écriture de la liste des lemmes\n";
		# liste des lemmes noms classés par fréquence décroissante
		print OUT "				<a name=\"noms_lemmatises\"></a>\n";
		print OUT "				<h1>Noms lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_nom\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		my @sortedLem=sort {$typeLemNom2freq{$b} <=> $typeLemNom2freq{$a}} keys %typeLemNom2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";

		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemNom2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des lemmes verbes classés par fréquence décroissante
		print OUT "				<a name=\"verbes_lemmatises\"></a>\n";
		print OUT "				<h1>Verbes lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_ver\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLemVer2freq{$b} <=> $typeLemVer2freq{$a}} keys %typeLemVer2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemVer2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des lemmes adjectifs classés par fréquence décroissante
		print OUT "				<a name=\"adjectifs_lemmatises\"></a>\n";
		print OUT "				<h1>Adjectifs lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_adj\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLemAdj2freq{$b} <=> $typeLemAdj2freq{$a}} keys %typeLemAdj2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemAdj2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des lemmes adverbes classés par fréquence décroissante
		print OUT "				<a name=\"adverbes_lemmatises\"></a>\n";
		print OUT "				<h1>Adverbes lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_adv\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLemAdv2freq{$b} <=> $typeLemAdv2freq{$a}} keys %typeLemAdv2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemAdv2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";


		# liste des lemmes classés par fréquence décroissante
		print OUT "				<a name=\"lemmes\"></a>\n";
		print OUT "				<h1>Lemmes classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_all\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th><th>Fr�q. dans le corpus de réf. (par M)</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLem2freq{$b} <=> $typeLem2freq{$a}} keys %typeLem2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				my $lem=$sortedLem[$i];
				Encode::_utf8_off($lem);
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLem2freq{$sortedLem[$i]}</td><td class=\"value\">$lemme_freq{$lem}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des formes class�es par fréquence décroissante
		print OUT "				<a name=\"formes\"></a>\n";
		print OUT "				<h1>Formes class�es par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"form_all\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Forme</th><th>Fréquence</th><th>Nombre de syllabes</th></thead>\n";
		print OUT "					<tbody>\n";
		my @sortedForm=sort {$type2freq{$b} <=> $type2freq{$a}} keys %type2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedForm ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedForm[$i])."</td><td class=\"value\">$type2freq{$sortedForm[$i]}</td><td class=\"value\">".nbSyll($sortedForm[$i])."</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		$verbose && print "concordances et pattern\n";
		# concordances et pattern
		print OUT qq|
					<a name="concord"></a>
					<h1>Recherche de concordance</h1>
					<p><label>Entrez un pivot (forme) ici (pattern accept�)<input size='40' id="pivot" value="le"/><button onclick="searchConcord(\$('#pivot').val())";>Rechercher</button>
					<br/>
					<small>
						<input type = "checkbox" checked="checked" id="motEntier"/> Mot Entier
						<input type = "checkbox" checked="false" id="caseSensitive"/> Sensibilit� � la casse
						<input type = "checkbox" checked="false" id="useLemmaConcord"/> Recherche de lemme<br/>
						Fen�tre = +/- <input type = "text" id="wordspan" value="40"  style='width:35px;' /> mots
						<p> Etiquettes reconnues : NOM, NPR, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation), TOK (token quelconque), NOSENT (token quelconque sauf la marque de fin de phrase).
						Les expressions r�guli�res sont permises (avec les métacaractères ".*+?" à l'intérieur des tokens et "(\|){}" � l'extérieur). Par exemple :
						<ul>
							<li><i>guerre TOK{1,4} paix</i> => pour les concordances de <i>guerre</i> suivi de <i>paix</i> avec de 1 � 4 tokens entre les deux.</li>
							<li><i>.*tion_NOM</i> => pour tous les noms se terminant par -tion</li>
							<li><i>d�.*_VER</i> => pour tous les verbes commençant par d�-</li>
						</ul>
					</small>
					<p>Occurrence(s) : <span id="nbocc"></span></p>
					<div id="concordTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="concordTable"></table>
					</div>
					<br/><br/>

					<a name="cooc"></a>
					<h1>Recherche de cooccurrences</h1>
					<p><label>Entrez un pattern compos� de lemmes ou d'�tiquettes <input size='40' id="pivot2" value="le"/><button onclick="searchCooc(\$('#pivot2').val())";>Rechercher</button>
					<br/>
					<small>
						Fen�tre = -<input type="text" style='width:35px;' id="fenLeft" value="5"/> / +<input type="text" style='width:35px;' id="fenRight" value="5"/> formes
						<input type = "checkbox" checked="checked" id="motEntierCooc"/> Mot Entier
						<input type = "checkbox" id="caseSensitiveCooc"/> Sensible � la casse
						<input type = "checkbox" checked="false" id="useLemmaCooc"/> Recherche de lemme
						<input type = "checkbox" id="insideSent"/> A l'intérieur des phrases
						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation)</p>
						<p>Nota bene : dans les indices ci-dessous le logarithme est calcul� en base 2</p>
					</small>

					<div id="coocTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="coocTable"></table>
					</div>
					<br/><br/>

					<a name="pattern"></a>
					<h1>Recherche de patterns</h1>
					<p><label>Entrez un pattern compos� de formes ou d'�tiquettes <input size='40' id="pat" value="DET NOM"/><button onclick="searchPattern(\$('#pat').val())";>Rechercher</button>
					<br/>
					<small>
						<input type = "checkbox" checked="checked" id="motEntierSearch"/> Mot Entier
						<input type = "checkbox" checked="false" id="caseSensitiveSearch"/> Sensibilit� � la casse
						<input type = "checkbox" checked="false" id="useLemmaSearch"/> Recherche de lemme

						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation)</p>
					</small>
					<div id="patternTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="patternTable"></table>
					</div>
					<br/><br/>

					<a name="seg_rep"></a>
					<h1>Recherche de segments répétés</h1>
					<p><label>Entrez un pattern compos� de formes ou d'�tiquettes <input size='40' id="patSegRep" value=".*"/><button onclick="searchSegRep(\$('#patSegRep').val())";>Rechercher</button>
					<br/>
					<small>
						Longueur min. <input type="text" style='width:35px;' id="longMinSegRep" value="3"/> - Fréquence min. <input type="text" style='width:35px;' id="freqMinSegRep" value="2"/>
						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation)</p>
					</small>
					<div id="segRepTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="segRepTable"></table>
					</div>

					<br/><br/>
	|;
		print OUT "				<p><b>Statistiques extraites en ".(time()-$now)." s</b></p>";


		print OUT "			</div>\n";

		# fermeture du corps

		print OUT "			<div id=\"footer\">";
		print OUT "				<a href=\"/anaText/doc/anatext.presentation.pdf\">Aide</a> - <i>Cr�dits : Olivier Kraif - Universit� Grenoble Alpes - Texte �tiquet� avec Treetagger (http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger)</i>\n";
		print OUT "			</div>";
		print OUT "		</div>\n";
		print OUT qq |
		<script type="text/javascript" language="javascript" src="js/lib/jquery-3.3.1.js"></script>
		<script type="text/javascript" language="javascript" src="js/lib/jquery.dataTables.min.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
		<script type="text/javascript" language="javascript" src="js/lib/highcharts.js"></script>
		<script type="text/javascript" language="javascript" src="js/$fileIn.txt.js"></script>
		<script type="text/javascript" language="javascript" src="js/$fileIn.tab.js"></script>
		<script type="text/javascript" language="javascript" src="js/lib/anaText.js"></script>
		<script type="text/javascript" language="javascript">var tranches=[$nbLemmesInconnus,$tranches];</script>
		<script type="text/javascript" language="javascript">var accVoc=[$accVoc];</script>
		|;
		print OUT "	</body>\n";
		print OUT "</html>\n";
		$verbose && print "fermeture\n";

	}

	#*********************************************************************************************************************** CS
	if ($outputFormat eq "csv") {

		print OUT qq |<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		"http://www.w3.org/TR/html4/strict.dtd">
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>Analyse lexicom�trique</title>
			<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
			<link rel="stylesheet" type="text/css" href="css/dataTables.tableTools.css" />
			<link rel="stylesheet" type="text/css" href="css/anaText.css" />
			<script type="text/javascript" language="javascript" src="js/lib/jquery-2.1.1.min.js"></script>
			<script type="text/javascript" language="javascript" src="js/lib/jquery.dataTables.js"></script>
			<script type="text/javascript" language="javascript" src="js/lib/dataTables.tableTools.js"></script>
			<script type="text/javascript" language="javascript" src="js/lib/highcharts.js"></script>
			<script type="text/javascript" language="javascript" src="js/".$fileIn.".txt.js"></script>
			<script type="text/javascript" language="javascript" src="js/".$fileIn.".tab.js"></script>
			<script type="text/javascript" language="javascript" src="js/lib/anaText.js"></script>
			<script type="text/javascript" language="javascript">var tranches=[$nbLemmesInconnus,$tranches];</script>

		</head>

		<body>
		|;


		print OUT "		<div id=\"corps\">\n";

		# écriture du menu

		print OUT qq |
				<div id="menu">
					<div class="titre">AnaText 2.3</div>
					<p class="link"><a href="texte.txt" target="blank">Texte source (utf-8)</a></p>
					<p class="link"><a href="texte.txt.ttg" target="blank">Texte �tiquet� (utf-8)</a></p>
					<ul>
						<li><a href="#statistiques_generales" class="gen">Statistiques g�n�rales</a></li>
						<li><a href="#mots_specifiques" class="gen">Formes spécifiques</a></li>
						<li><a href="#accroissement_vocabulaire" class="gen">Accroissement du vocabulaire</a></li>
						<li><a href="#tranches_de_frequence" class="gen">Tranches de fréquence</a></li>
						<li><a href="#morphologie_verbale"  class="morpho">Morphologie verbale</a></li>
						<li><a href="#noms_lemmatises" class="freq">Noms lemmatisés</a></li>
						<li><a href="#verbes_lemmatises"  class="freq">Verbes lemmatisés</a></li>
						<li><a href="#adjectifs_lemmatises"  class="freq">Adjectifs lemmatisés</a></li>
						<li><a href="#adverbes_lemmatises"  class="freq">Adverbes lemmatisés</a></li>
						<li><a href="#lemmes"  class="freq">Tous les lemmes</a></li>
						<li><a href="#formes"  class="freq">Toutes les formes</a></li>
						<li><a href="#seg_rep"  class="pat">Segments répétés</a></li>
					</ul>
					<br/>
					<center><button id='save'>Sauvegarder</button></center>
				</div>
				<div id="lecture">
					<div id='page'></div>
					<div id='pageN'>Page <span id="pageNum"></span>/<span id="pageTot"></span></div>
					<center><img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/back_enabled.png"  id="back"/> <img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/forward_enabled.png" id="forward"/></center>
					<img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/back_enabled_hover.png"  id="back_hover"/> <img src="http://phraseotext.univ-grenoble-alpes.fr/anaText/images/forward_enabled_hover.png" id="forward_hover"/>
				</div>

	|;

		#ecriture des statistiques

		print OUT "			<div id=\"statistiques\">\n";
		print OUT "				<p class=\"titre\">Texte analys&eacute; : <span id='title'>$title</span></p>\n";
		print OUT "				<a style=\"color:red;font-size:12px\" onmouseover=\"this.style.cursor='pointer'\" onclick=\"alert('Pour ne pas encombrer le serveur, les résultats de l\\'analyse sont supprimés au bout de 24h : pour les conserver localement, faites \\'enregistrez-sous\\' dans votre navigateur, apr�s avoir cliqu� sur le bouton \\'Sauvegarder\\' en bas du menu\\n');\">Lire l'avertissement</a><br/>";
		print OUT "				<a name=\"statistiques_generales\"></a>\n";
		print OUT "				<h1>Statistiques g&eacute;n&eacute;rales</h1>\n";
		print OUT "				<table>\n";
		print OUT "					<tr><td colspan=\"2\" class=\"tableTitle\">Occurrences</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Phrases </td><td class=\"value\">$nbSent</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Tokens (formes et ponctuation)</td><td class=\"value\">$nbTok</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Formes</td><td class=\"value\">$nbForm</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Syllabes</td><td class=\"value\">$nbSyll</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Caract&egrave;res (hors ponctuation)</td><td class=\"value\">$nbCar</td></tr>\n";

		print OUT "					<tr><td colspan=\"2\" class=\"tableTitle\">Lisibilité</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Nombre moyen de formes par phrases </td><td class=\"value\">".sprintf("%.1f",$nbForm/$nbSent)."</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Nombre moyen de syllabes par forme </td><td class=\"value\">".sprintf("%.1f",$nbSyll/$nbForm)."</td></tr>\n";

		print OUT "					<tr><td colspan=\"2\" class=\"tableTitle\">Types</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Formes</td><td class=\"value\">$nbType</td></tr>\n";
		print OUT "					<tr><td class=\"label\">Lemmes</td><td class=\"value\">$nbTypeLemme</td></tr>\n";
		print OUT "				</table>\n";


		# mots specifiques
		print OUT "				<a name=\"mots_specifiques\"></a>\n";
		print OUT "				<h1>Formes sp&eacute;cifiques</h1>\n";
		print OUT "				<table  id=\"specifiques\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fr&eacute;quence</th><th>CorpusRef (par million)</th><th>Ratio de fréquences (spécificité)</th></thead>\n";
		print OUT "					<tbody>\n";

		my $count=0;
		open (CSV_KEYWORDS,">:encoding(utf8)",$fileIn.".keywords.csv");
		print CSV_KEYWORDS "Rang\tLemme\tFé�quence\tCorpusRef (par M)\tRatio de fréquence\n";

		foreach $lemme (@keywords) {
			if ($lemme) {
				$count+=1;
				print OUT "						<tr><td class=\"label\">$count</td><td class=\"value\">".toXML($lemme)."</td><td class=\"value\">$freq{$lemme}</td><td class=\"value\">$lemme_freq{$lemme}</td><td class=\"value\">". sprintf("%.3f", $specificite{$lemme})."</td></tr>\n";
				print CSV_KEYWORDS "$count\t$lemme\t$freq{$lemme}\t$lemme_freq{$lemme}\t".sprintf("%.3f", $specificite{$lemme})."\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";
		print OUT "				<a href=\"$fileIn.keywords.csv\">T�l�charger le fichier CSV</a>\n";
		close (CSV_KEYWORDS);


		#tranches de fréquence
		open (CSV_FREQRANGE,">:encoding(utf8)",$fileIn.".freqrange.csv");
		print CSV_FREQRANGE "Tranche\tNombre d'occurrences\n";
		print OUT "				<a name=\"tranches_de_frequence\"></a>\n";
		print OUT "				<h1>Tranches de fréquence</h1>\n";
		print OUT "				<table id=\"tranches\" class=\"display\">\n";
		print OUT "					<thead><th>Tranche</th><th>Nombre d'occurrences</th></thead>\n";
		print OUT "					<tbody>\n";
		# d�finition de l'intervalle de fréquence
		my $inf=0;
		my $sup=1000;
		my $newSup;
		for (my $i=0;$i<=$#tranche_nb;$i++) {
			if (exists( $tranche_nb[$i])) {
				print OUT "						<tr><td class=\"label\">Tranche ".($i+1)."<br/>$inf &lt;= Rang &lt; $sup</td><td class=\"value\">$tranche_nb[$i]</td></tr>\n";
				print CSV_FREQRANGE "Tranche ".($i+1)."\t$inf\t$sup\t$tranche_nb[$i]\n";
			}

			$newSup=$sup+2*($sup-$inf);
			$inf=$sup;
			$sup=$newSup;
		}
		print OUT "						<td class=\"label\">Absents du corpus de référence</td><td class=\"value\">$nbLemmesInconnus</td>\n";
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";
		print OUT "				<div id=\"histo_freq\"></div>\n";
		print OUT "				<a href=\"$fileIn.freqrange.csv\">T�l�charger le fichier CSV</a>\n";
		close(FREQRANGE);


		# morphosyntaxe
		open (CSV_MORPHOV,">:encoding(utf8)",$fileIn.".morphov.csv");
		print CSV_MORPHOV "Temps / mode\tNombre d'occurrences\n";
		print OUT "				<a name=\"morphologie_verbale\"></a>\n";
		print OUT "				<h1>Morphologie verbale</h1>\n";
		print OUT "				<table  id=\"morpho_v\" class=\"display\">\n";
		print OUT "					<thead><tr><th>Temps / mode</th><th>Nombre d'occurrences</th></tr></thead>\n";
		print OUT "					<tbody>\n";
		foreach my $msd (sort keys %{$statsMsd{'VER'}}) {
			print OUT "						<tr><td class=\"label\">$msd</td><td class=\"value\">".$statsMsd{'VER'}{$msd}."</td></tr>\n";
			print CSV_MORPHOV "$msd\t".$statsMsd{'VER'}{$msd}."\n";
		}

		#~ print OUT "						<tr><td class=\"label\">Infinitif</td><td class=\"value\">$nbinf</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif pr&eacute;sent</td><td class=\"value\">$nbindpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif imparfait</td><td class=\"value\">$nbindimp</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif pass&eacute; simple</td><td class=\"value\">$nbindpsi</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Indicatif futur</td><td class=\"value\">$nbindfut</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Subjonctif pr&eacute;sent</td><td class=\"value\">$nbsubpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Subjonctif pass&eacute;</td><td class=\"value\">$nbsubpas</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Conditionnel pr&eacute;sent</td><td class=\"value\">$nbconpre</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Participe pr&eacute;sent</td><td class=\"value\">$nbppr</td></tr>\n";
		#~ print OUT "						<tr><td class=\"label\">Participe pass&eacute;</td><td class=\"value\">$nbppa</td></tr>\n";
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";
		close(CSV_MORPHOV);
		print OUT "				<a href=\"$fileIn.morphov.csv\">T�l�charger le fichier CSV</a>\n";

		# liste des lemmes noms classés par fréquence décroissante

		open (CSV_LEM_NOMS,">:encoding(utf8)",$fileIn.".lem_noms.csv");
		print CSV_LEM_NOMS "Rang\tLemme\t><th>Fréquence</th></thead>\n";
		print OUT "				<a name=\"noms_lemmatises\"></a>\n";
		print OUT "				<h1>Noms lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_nom\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		my @sortedLem=sort {$typeLemNom2freq{$b} <=> $typeLemNom2freq{$a}} keys %typeLemNom2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";

		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemNom2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des lemmes verbes classés par fréquence décroissante
		print OUT "				<a name=\"verbes_lemmatises\"></a>\n";
		print OUT "				<h1>Verbes lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_ver\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLemVer2freq{$b} <=> $typeLemVer2freq{$a}} keys %typeLemVer2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemVer2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des lemmes adjectifs classés par fréquence décroissante
		print OUT "				<a name=\"adjectifs_lemmatises\"></a>\n";
		print OUT "				<h1>Adjectifs lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_adj\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLemAdj2freq{$b} <=> $typeLemAdj2freq{$a}} keys %typeLemAdj2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemAdj2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des lemmes adverbes classés par fréquence décroissante
		print OUT "				<a name=\"adverbes_lemmatises\"></a>\n";
		print OUT "				<h1>Adverbes lemmatisés classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_adv\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLemAdv2freq{$b} <=> $typeLemAdv2freq{$a}} keys %typeLemAdv2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLemAdv2freq{$sortedLem[$i]}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";


		# liste des lemmes classés par fréquence décroissante
		print OUT "				<a name=\"lemmes\"></a>\n";
		print OUT "				<h1>Lemmes classés par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"lemma_all\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Lemme</th><th>Fréquence</th><th>Fr�q. dans le corpus de réf. (par M)</th></thead>\n";
		print OUT "					<tbody>\n";
		@sortedLem=sort {$typeLem2freq{$b} <=> $typeLem2freq{$a}} keys %typeLem2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedLem ; $i++) {
				my $lem=$sortedLem[$i];
				Encode::_utf8_off($lem);
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedLem[$i])."</td><td class=\"value\">$typeLem2freq{$sortedLem[$i]}</td><td class=\"value\">$lemme_freq{$lem}</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# liste des formes class�es par fréquence décroissante
		print OUT "				<a name=\"formes\"></a>\n";
		print OUT "				<h1>Formes class�es par fréquence décroissante</h1>\n";
		print OUT "				<table id=\"form_all\" class=\"display\">\n";
		print OUT "					<thead><th>Rang</th><th>Forme</th><th>Fréquence</th><th>Nombre de syllabes</th></thead>\n";
		print OUT "					<tbody>\n";
		my @sortedForm=sort {$type2freq{$b} <=> $type2freq{$a}} keys %type2freq;
		if (@sortedLem==0) {
				print OUT "						<tr><td class=\"label\">&nbsp;</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td><td class=\"value\">Pas de résultat</td></tr>\n";
		}else{
			for (my $i=0;$i<=$#sortedForm ; $i++) {
				print OUT "						<tr><td class=\"label\">".($i+1)."</td><td class=\"value\">".toXML($sortedForm[$i])."</td><td class=\"value\">$type2freq{$sortedForm[$i]}</td><td class=\"value\">".nbSyll($sortedForm[$i])."</td></tr>\n";
			}
		}
		print OUT "					</tbody>\n";
		print OUT "				</table>\n";
		print OUT "				<br/><br/>\n\n";

		# concordances et pattern
		print OUT qq|
					<a name="concord"></a>
					<h1>Recherche de concordance</h1>
					<p><label>Entrez un pivot (forme) ici (pattern accept�)<input size='40' id="pivot" value="le"/><button onclick="searchConcord(\$('#pivot').val())";>Rechercher</button>
					<br/>
					<small>
						<input type = "checkbox" checked="checked" id="motEntier"/> Mot Entier
						<input type = "checkbox" checked="false" id="caseSensitive"/> Sensibilit� � la casse
						<input type = "checkbox" checked="false" id="useLemmaConcord"/> Recherche de lemme<br/>
						Fen�tre = +/- <input type = "text" id="wordspan" value="40"  style='width:35px;' /> mots
						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation), TOK (token quelconque), NOSENT (token quelconque sauf la marque de fin de phrase).
						Les expressions r�guli�res sont permises (avec les métacaractères ".*+?" à l'intérieur des tokens et "(\|){}" à l'extérieur). Par exemple :
						<ul>
							<li><i>guerre TOK{1,4} paix</i> => pour les concordances de <i>guerre</i> suivi de <i>paix</i> avec de 1 à 4 tokens entre les deux.</li>
							<li><i>.*tion_NOM</i> => pour tous les noms se terminant par -tion</li>
							<li><i>d�.*_VER</i> => pour tous les verbes commençant par d�-</li>
						</ul>
					</small>
					<p>Occurrence(s) : <span id="nbocc"></span></p>
					<div id="concordTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="concordTable"></table>
					</div>
					<br/><br/>

					<a name="cooc"></a>
					<h1>Recherche de cooccurrences</h1>
					<p><label>Entrez un pattern compos� de lemmes ou d'�tiquettes <input size='40' id="pivot2" value="le"/><button onclick="searchCooc(\$('#pivot2').val())";>Rechercher</button>
					<br/>
					<small>
						Fen�tre = +/- <input type="text" style='width:35px;' id="fen" value="5"/> formes
						<input type = "checkbox" checked="checked" id="motEntierCooc"/> Mot Entier
						<input type = "checkbox" id="caseSensitiveCooc"/> Sensible � la casse
						<input type = "checkbox" checked="false" id="useLemmaCooc"/> Recherche de lemme
						<input type = "checkbox" id="insideSent"/> A l'intérieur des phrases
						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation)</p>
						<p>Nota bene : dans les indices ci-dessous le logarithme est calcul� en base 2</p>
					</small>

					<div id="coocTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="coocTable"></table>
					</div>
					<br/><br/>

					<a name="pattern"></a>
					<h1>Recherche de patterns</h1>
					<p><label>Entrez un pattern compos� de formes ou d'�tiquettes <input size='40' id="pat" value="DET NOM"/><button onclick="searchPattern(\$('#pat').val())";>Rechercher</button>
					<br/>
					<small>
						<input type = "checkbox" checked="checked" id="motEntierSearch"/> Mot Entier
						<input type = "checkbox" checked="false" id="caseSensitiveSearch"/> Sensibilit� � la casse
						<input type = "checkbox" checked="false" id="useLemmaSearch"/> Recherche de lemme

						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation)</p>
					</small>
					<div id="patternTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="patternTable"></table>
					</div>
					<br/><br/>

					<a name="seg_rep"></a>
					<h1>Recherche de segments répétés</h1>
					<p><label>Entrez un pattern compos� de formes ou d'�tiquettes <input size='40' id="patSegRep" value=".*"/><button onclick="searchSegRep(\$('#patSegRep').val())";>Rechercher</button>
					<br/>
					<small>
						<p> Etiquettes reconnues : NOM, VER, ADJ, ADV, DET, PRO, PRE, CON, NUM (nombre), PHR (fin de phrase), PON (ponctuation)</p>
					</small>
					<div id="segRepTableDiv">
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="segRepTable"></table>
					</div>

					<br/><br/>
	|;
		print OUT "				<p><b>Statistiques extraites en ".(time()-$now)." s</b></p>";


		print OUT "			</div>\n";

		# fermeture du corps

		print OUT "			<div id=\"footer\">";
		print OUT "				<i>Cr�dits : Olivier Kraif - Universit� Stendhal Grenoble 3 - Texte �tiquet� avec Treetagger (http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger)</i>\n";
		print OUT "			</div>";
		print OUT "		</div>\n";
		print OUT "	</body>\n";
		print OUT "</html>\n";

	}


	close(IN);

}


# conversion des entit�s

sub toXML {
	my $chaine=shift;
	$chaine=~s/&/&amp;/g;
	$chaine=~s/"/&quot;/g;
	$chaine=~s/</&lt;/g;
	$chaine=~s/>/&gt;/g;
	$chaine=~s/'/&apos;/g;
	return $chaine;
}

# calcul de loglike
sub computeLL {
	my %am;

	my ($n1,$n2,$n12,$N)=@_;
	if ($n12*$n1*$n2*$N==0) {
		$am{"am.log.likelihood"}=0;
		$verbose && print "Score nul : n1=$n1, n2=$n2, n12=$n12, N=$N\n";
		return 0;
	}
	print "Calcul n1=$n1, n2=$n2, n12=$n12, N=$N\n";
	my $ePP=$n1*$n2/$N;
	my $ePA=$n1*($N-$n2)/$N;
	my $eAP=($N-$n1)*$n2/$N;
	my $eAA=($N-$n1)*($N-$n2)/$N;

	my $lPP=$n12*log($n12/$ePP);
	my $lPA=($n1-$n12>0)?($n1-$n12)*log(($n1-$n12)/$ePA):0;
	my $lAP=($n2-$n12>0)?($n2-$n12)*log(($n2-$n12)/$eAP):0;
	my $lAA=($N-$n1-$n2+$n12>0)?($N-$n1-$n2+$n12)*log(($N-$n1-$n2+$n12)/$eAA):0;

	my $coeff=($n12>=$ePP)? 1:-1;
	return $coeff*2*($lPP+$lPA+$lAP+$lAA);
}

#******************************************************************************************************************************** Main

ttg2stats($fileIn);
