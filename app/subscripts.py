# coding : utf-8

#Lanceur de ligne de commande
import subprocess
#Parcours de fichiers
import os
#Expressions régulières
import re
#Lecture de XML
import lxml
from lxml import etree
#Date et heure, pour la création de noms de fichiers
import datetime
#Analyseur SpaCy
import spacy


"""
check_parameters(lang,prog,configs)
Cette fonction permet de vérifier la validité des informations entrées par l'utilisateur,
que ce soit à travers la ligne de commande ou à travers le fichier de configuration.
Elle vérifie que le code de programme entré correspond bien à un programme supporté
par l'outil, que la langue entrée fait bien partie des langues traitées par le programme
voulu, et que le fichier de configuration contient bien toutes les informations
nécessaires.
Entrees:
lang: Indique la langue de TreeTagger que l'utilisateur veut lancer (string)
prog: Nom du programme à lancer (string)
configs: paramètres définis par l'utilisateur, extraits du fichier de configuration (dict)
Sorties:
prog: Nom du programme à lancer (string)
valid_langs: Langues valide pour l'outil choisi, et les commandes du dit outil
    qui lui sont liées (dict)
path: Chemin d'accès a l'outil demandé (string)
models: Pour UDPipe, chemin d'accès au dossier de modèles (string)
"""

def check_parameters(lang,prog,configs):
    #Définition d'une liste des programmes valides
    valid_progs={"Treetagger":"TTG","TreeTagger":"TTG","treetagger":"TTG"\
    ,"TTG":"TTG","UDpipe":"UDP","UDP":"UDP","UDPipe":"UDP","udpipe":"UDP"\
    ,"Talismane":"TAL","talismane":"TAL","TAL":"TAL","SpaCy":"SPC","Spacy":"SPC"\
    ,"spacy":"SPC","SPC":"SPC","CoreNLP":"NLP","corenlp":"NLP","NLP":"NLP"}
    #On vérifie que le programme entré par l'utilisateur fait bien partie de ceux
    #implémentés
    if prog not in valid_progs.keys():
        raise Exception("Programme non supporté ou commande de programme incorrect")
    else:
        prog=valid_progs[prog]
    #Pour chaque étiqueteur, construction du tableau des options entrées par l'utilisateur
    #cas TreeTagger
    if prog=="TTG":
        try:
            path=configs["TTG_Path"].strip()
            valid_langs={"bul":"bulgarian","cat":"catalan","ces":"czech","dan":"danish"\
            ,"nld":"dutch","eng":"english","est":"estonian","fin":"finnish","fra":"french"\
            ,"glg":"galician","deu":"german","ell":"greek","ita":"italian","kor":"korean"\
            ,"lat":"latin","nob":"norwegian","pol":"polish","por":"portuguese","ron":"romanian"\
            ,"rus":"russian","slk":"slovak","slv":"slovenian","spa":"spanish","swa":"swahili"\
            ,"swe":"swedish","grc":"ancient-greek-utf8","gmh":"middle-high-german"\
            ,"fro":"old-french"}
        except Exception as e:
            raise Exception("Fichier de configuration mal formé pour TreeTagger")
        if lang not in valid_langs.keys():
            raise Exception("Langage incorrect ou non supporté par TreeTagger")
        return(prog,path,valid_langs)
    #cas UDPipe
    elif prog=="UDP":
        try:
            path=configs["UDPipe_Path"].strip()
            models=configs["UDPipe_Models"].strip()
            valid_langs={}
            for key in configs.keys():
                if key.startswith("UDPipe_Lang_"):
                    valid_langs[re.sub("UDPipe_Lang_","",key.strip())]=configs[key].strip()
        except Exception as e:
            raise Exception("Fichier de configuration mal formé pour UDPipe")
        if lang not in valid_langs.keys():
            raise Exception("Langage incorrect ou non supporté par UDPipe")
        return(prog,path,valid_langs,models)
    #cas Talismane
    elif prog=="TAL":
        try:
            path=configs["Talismane_Path"].strip()
            valid_langs={"fra":("talismane-fr-5.2.0.conf","fr"),"eng":("talismane-en-5.2.0.conf","en")}
        except Exception as e:
            raise Exception("Fichier de configuration mal formé pour Talismane")
        if lang not in valid_langs.keys():
            raise Exception("Langage incorrect ou non supporté par Talismane")
        return(prog,path,valid_langs)
    #Cas SpaCy
    elif prog=="SPC":
        try:
            valid_langs={"eng":"en_core_web_sm","deu":"de_core_news_sm"\
            ,"fra":"fr_core_news_sm","spa":"es_core_news_sm","por":"pt_core_news_sm"\
            ,"ita":"it_core_news_sm","nld":"nl_core_news_sm","ell":"el_core_news_sm"\
            ,"nob":"nb_core_news_sm","lit":"lt_core_news_sm"}
        except Exception as e:
            raise Exception("Fichier de configuration mal formé pour SpaCy")
        if lang not in valid_langs.keys():
            raise Exception("Langage incorrect ou non supporté par SpaCy")
        return(prog,valid_langs)
    #Cas CoreNLP
    elif prog=="NLP":
        try:
            path=configs["Core_Path"].strip()
            valid_langs={"fra":("french","stanford-french-corenlp-2018-10-05-models.jar")\
            ,"eng":("english","stanford-english-corenlp-2018-10-05-models.jar")\
            ,"deu":("german","stanford-german-corenlp-2018-10-05-models.jar")\
            ,"spa":("spanish","stanford-spanish-corenlp-2018-10-05-models.jar")\
            ,"ara":("arabic","stanford-arabic-corenlp-2018-10-05-models.jar")\
            ,"cmn":("chinese","stanford-chinese-corenlp-2018-10-05-models.jar")}
        except Exception as e:
            raise Exception("Fichier de configuration mal formé pour CoreNLP")
        if lang not in valid_langs.keys():
            raise Exception("Langage incorrect ou non supporté par CoreNLP")
        return(prog,path,valid_langs)

"""
run_tagger(itempath,prog,lang,exts,inplace,keepstruct,results,valid_langs,path="N/A",models="N/A")
Cette fonction permet de lancer l'analyseur choisi sur le ou les fichiers indiqués.
Entrees:
itempath: Chemin d'accès relatif vers l'objet à traiter(string)
prog: Indique le programme à utiliser (string)
lang: Indique la langue de TreeTagger que l'utilisateur veut lancer (string)
exts: Indique la ou les extensions de fichiers sur lesquels le script doit se lancer (list)
inplace: Indique si le traitement des fichiers XML doit se faire dans une copie
    du fichier original au format XML (boolean)
keepstruct: Indique la balise qui doit être utilisée comme balise minimum pour
    l'analyse avancée XML (string)
check_result: Résultats de l'analyse des informations données par l'utilisateur (tuple)
results: Tableau contenant les résulats des étiqueteurs (list)
valid_langs: Tableau contenant les langues valables pour l'outil choisi (dict)
ana: Indique si l'utilisateur souhaite lancer AnaText sur les résultats (boolean)
path: Chemin d'accès vers l'outil à utiliser si nécessaire (string)
models: Chemin d'accès vers le dossier de modèles pour UDPipe
Sortie:
Création d'un ou de plusieurs fichiers contenant les résultats demandés
"""

def run_tagger(itempath,prog,lang,exts,inplace,keepstruct,results,valid_langs,ana,path="N/A",models="N/A"):
    #TEMP
    #Dans l'attente d'une meilleure solution, les tags inconnus de TTG seront mis sous NAM
    UD_to_TTG={"ADJ":"ADJ","ADP":"PRP","ADV":"ADV","AUX":"VER","CONJ":"KON"\
    ,"CCONJ":"KON","DET":"DET","INTJ":"INT","NOUN":"NOM","NUM":"NUM"\
    ,"PART":"NAM","PRON":"PRO","PROPN":"NAM","PUNCT":"PUN","SCONJ":"KON"\
    ,"SYM":"SYM","VERB":"VER","X":"NAM"}
    TAL_to_TTG={"ADJ":"ADJ","ADV":"ADV","ADVWH":"ADV","CC":"KON","CLO":"PRO"\
    ,"CLR":"PRO","CLS":"PRO","CS":"KON","DET":"DET","DETWH":"DET"\
    ,"ET":"NAM","I":"INT","NC":"NOM","NPP":"NAM","P":"PRP","P+D":"PRP:det"\
    ,"P+PRO":"PRP","PONCT":"PUN","PREF":"NAM","PRO":"PRO","PRORE":"PRO:REL"\
    ,"PROWH":"PRO","V":"VER","VIMP":"VER:impe","VINF":"VER:infi"\
    ,"VPP":"VER:pper","VPR":"VER:ppre","VS":"VER"}
    #TEMP
    #On vérifie que le fichier donné possède bien l'une des extensions demandées
    if itempath.endswith(tuple(exts)):
        #Traitement séparé pour les fichiers .xml
        if itempath.endswith(".xml"):
            if keepstruct!="n/a":
                parser = etree.XMLParser()
                tree=etree.parse(itempath,parser)
                ex_path = etree.ETXPath('//{http://www.tei-c.org/ns/1.0}'+keepstruct)
                ex_path_2 = etree.ETXPath('string()')
                source=ex_path(tree)
                if source is None:
                    raise Exception("Elément demandé non trouvé")
                for element in source:
                    texte_interne=ex_path_2(element)
                    texte_interne=re.sub("\n","",texte_interne)
                    texte_interne=re.sub(" {2,}"," ",texte_interne)
                    time=str(datetime.datetime.now())
                    time=re.sub(" ","",time)
                    name=time+"_temp.temp"
                    temp_file=open(name,mode="w",encoding="utf-8")
                    temp_file.write(texte_interne)
                    temp_file.close()
                    #Cas TreeTagger
                    if prog=="TTG":
                        result=subprocess.check_output([path+'/cmd/tree-tagger-'+valid_langs[lang],name])
                        TTG_result=result.decode("UTF-8")
                        TTG_result=TTG_result.split("\n")
                        results=[[]]
                        for part in TTG_result:
                            if len(part.split("\t"))==3:
                                results[0].append(part.split("\t"))
                    #Cas UDPipe
                    elif prog=="UDP":
                        result=subprocess.check_output([path+'/udpipe'\
                        ,"--tokenize","--tag","--parse",models+"/"+valid_langs[lang]\
                        ,name])
                        UDP_result=result.decode("UTF-8")
                        UDP_result=UDP_result.split("\n")
                        results=[]
                        pos=-1
                        for line in UDP_result:
                            if line.startswith("# sent_id"):
                                #add a new empty list for each sentence
                                results.append([])
                                pos+=1
                            elif not line.startswith("#") and len(line.split("\t"))>2:
                                results[pos].append(line.split("\t"))
                    #Cas Talismane
                    elif prog=="TAL":
                        temp2=re.sub(" ","",str(datetime.datetime.now()))+"_temp.txt"
                        subprocess.run(["java","-Xmx1G","-Dconfig.file="+path+"/"+valid_langs[lang][0]\
                        ,"-jar",path+"/talismane-core-5.3.0.jar","--analyse","--sessionId="+valid_langs[lang][1]\
                        ,"--encoding=UTF8","--inFile="+name,"--outFile="+temp2])
                        #Talismane ne nous permet pas de récupèrer la sortie depuis la console
                        #on l'enregistre donc dans un nouveau fichier temporaire
                        temp2_file=open(temp2,mode="r",encoding="UTF-8")
                        results=[[]]
                        pos=0
                        newsent=True
                        for line in temp2_file.readlines():
                            if len(line)==1 and newsent:
                                #Ajout d'une nouvelle liste vide pour chaque phrase
                                results.append([])
                                pos+=1
                                #On empêche la création de phrases vides
                                newsent=False
                            elif len(line.split("\t"))>2:
                                results[pos].append(line.split("\t"))
                                newsent=True
                        temp2_file.close()
                        os.remove(temp2)
                    #Cas SpaCy
                    elif prog=="SPC":
                        annotateur = spacy.load(valid_langs[lang])
                        source_file=open(name,mode="r",encoding="UTF-8")
                        source_text=source_file.read()
                        source_file.close()
                        SPC_result = annotateur(source_text)
                        results=[[]]
                        for token in SPC_result:
                            if token.pos_!="SPACE":
                                results[0].append([token.text,token.lemma_,token.pos_])
                    #Cas CoreNLP
                    elif prog=="NLP":
                        subprocess.run(["java","-cp",path+"/stanford-corenlp-3.9.2.jar:"\
                        +path+"/"+valid_langs[lang][1],"-Xmx8g"\
                        ,"edu.stanford.nlp.pipeline.StanfordCoreNLP","-props"\
                        ,"StanfordCoreNLP-"+valid_langs[lang][0]+".properties"\
                        ,"-annotators","tokenize,ssplit,pos,lemma","-file"\
                        ,name,"-outputFormat","text"])
                        #CoreNLP ne nous permet pas de choisir le fichier de sortie
                        #et crée toujours la sortie dans un fichier file.out
                        #dans le dossier depuis lequel CoreNLP est lancé
                        #Il est donc nécessaire de récupèrer le contenu de ce dossier
                        output_filename=name+".out"
                        output_file=open(output_filename,mode="r",encoding="UTF-8")
                        result=output_file.read()
                        output_file.close()
                        os.remove(output_filename)
                        NLP_result=result.split("\n")
                        results=[]
                        pos=-1
                        for part in NLP_result:
                            if part.startswith("Sentence #"):
                                #On ajoute une nouvelle liste vide pour chaque
                                #phrase
                                results.append([])
                                pos+=1
                            elif part.startswith("["):
                                part=part.strip("[]")
                                part=part.split(" ")
                                results[pos].append(part)
                    os.remove(name)
                    new_elt_1=etree.SubElement(element.getparent(),keepstruct)
                    sent_nb=0
                    for sent in results:
                        sent_nb+=1
                        new_elt_2=etree.SubElement(new_elt_1,"s",attrib={"sentence_id":str(sent_nb)})
                        tok_nb=0
                        for word in sent:
                            tok_nb+=1
                            if prog=="TTG":
                                new_elt_3=etree.SubElement(new_elt_2,"w",attrib={"token_id":str(tok_nb),"lemma":word[2],"pos":word[1]})
                                new_elt_3.text=word[0]
                            elif prog=="UDP":
                                new_elt_3=etree.SubElement(new_elt_2,"w",attrib={"token_id":str(tok_nb),"lemma":word[2],"pos":word[3]})
                                new_elt_3.text=word[1]
                            elif prog=="TAL":
                                new_elt_3=etree.SubElement(new_elt_2,"w",attrib={"token_id":str(tok_nb),"lemma":word[2],"pos":word[3]})
                                new_elt_3.text=word[1]
                            elif prog=="SPC":
                                new_elt_3=etree.SubElement(new_elt_2,"w",attrib={"token_id":str(tok_nb),"lemma":word[1],"pos":word[2]})
                                new_elt_3.text=word[0]
                            elif prog=="NLP":
                                lemma=word[4].split("=")
                                pos=word[3].split("=")
                                text=word[0].split("=")
                                new_elt_3=etree.SubElement(new_elt_2,"w",attrib={"token_id":str(tok_nb),"lemma":lemma[1],"pos":pos[1]})
                                new_elt_3.text=text[1]
                    element.getparent().remove(element)
                new_name="output/"+prog+"_XML_tagged_"+itempath.split("/")[-1]
                tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
                parser = etree.XMLParser(remove_blank_text=True)
                tree=etree.parse(new_name,parser)
                tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
            else:
                try:
                    tree=etree.parse(itempath)
                    root=tree.getroot()
                    #On récupère ce qui ce trouve dans la balise "text" ou "texte"
                    source=root.find('{http://www.tei-c.org/ns/1.0}text')
                    if source is None:
                        source=root.find('{http://www.tei-c.org/ns/1.0}texte')
                    try:
                        tree_text=etree.tostring(source, encoding='utf-8', method='text')
                    except Exception as e:
                        raise Exception("Etiquette 'text' non trouvée")
                    tree_text=tree_text.decode(encoding="utf-8")
                    #On crée un fichier temporaire qui contient le texte brut extrait de l'arbre XML
                    #On utilise la date actuelle pour nommer ce fichier afin
                    #de minimiser le risque de conflit de nom de fichiers
                    time=re.sub(" ","",str(datetime.datetime.now()))
                    name=time+"_temp.temp"
                    temp_file=open(name,mode="w",encoding="utf-8")
                    temp_file.write(tree_text)
                    temp_file.close
                    #Pour chaque analyseur, on le lance sur le fichier XML, puis on récupère
                    #les résultats pour les restituer sous la forme demandée
                    #Cas TreeTagger
                    if prog=="TTG":
                        result=subprocess.check_output([path+'/cmd/tree-tagger-'+valid_langs[lang],name])
                        if inplace:
                            TTG_result=result.decode("UTF-8")
                            TTG_result=TTG_result.split("\n")
                            TTG_results=[]
                            for element in TTG_result:
                                if len(element.split("\t"))==3:
                                    TTG_results.append(element.split("\t"))
                            lecteur = etree.XMLParser(remove_blank_text=True)
                            old_tree=etree.parse(itempath,lecteur)
                            new_elt=etree.SubElement(old_tree.getroot(),"text",attrib={"id":"taggedtext"})
                            for token in TTG_results:
                                newer_elt=etree.SubElement(new_elt,"w",attrib={"lemma":token[2],"pos":token[1]})
                                newer_elt.text=token[0]
                            new_name="output/TTG_tagged_"+itempath.split("/")[-1]
                            old_tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
                        elif ana:
                            #AnaText attend des résultats TTG, donc pas besoin de modification
                            results.append((itempath,result.decode("UTF-8"),"TreeTagger"))
                        else:
                            results.append((itempath,result.decode("UTF-8"),"TreeTagger"))
                    #Cas UDPipe
                    elif prog=="UDP":
                        result=subprocess.check_output([path+'/udpipe'\
                        ,"--tokenize","--tag","--parse",models+"/"+valid_langs[lang]\
                        ,name])
                        if inplace or ana:
                            UDP_result=result.decode("UTF-8")
                            UDP_result=UDP_result.split("\n")
                            UDP_results=[]
                            for element in UDP_result:
                                if len(element.split("\t"))>2:
                                    UDP_results.append(element.split("\t"))
                            if inplace:
                                lecteur = etree.XMLParser(remove_blank_text=True)
                                old_tree=etree.parse(itempath,lecteur)
                                new_elt=etree.SubElement(old_tree.getroot(),"text",attrib={"id":"taggedtext"})
                                for token in UDP_results:
                                    newer_elt=etree.SubElement(new_elt,"w",attrib={"lemma":token[2],"pos":token[3]})
                                    newer_elt.text=token[1]
                                new_name="output/UDP_tagged_"+itempath.split("/")[-1]
                                old_tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
                            elif ana:
                                ana_UDP=""
                                convert_pos=""
                                for token in UDP_results:
                                    if token[3].strip() in UD_to_TTG.keys():
                                        convert_pos=UD_to_TTG[token[3].strip()]
                                    else:
                                        convert_pos="NAM"
                                    ana_line=token[1]+"\t"+convert_pos+"\t"+token[2]
                                    ana_UDP=ana_UDP+ana_line+"\n"
                                results.append((itempath,ana_UDP,"UDPipe"))
                        else:
                            results.append((itempath,result.decode("UTF-8"),"UDPipe"))
                    #Cas Talismane
                    elif prog=="TAL":
                        #On a besoin de créer un second fichier temporaire
                        #car Talismane ne nous permet pas de récupèrer la sortie
                        #depuis stdout
                        temp2=re.sub(" ","",str(datetime.datetime.now()))+"_temp.txt"
                        subprocess.run(["java","-Xmx1G","-Dconfig.file="+path+"/"+valid_langs[lang][0]\
                        ,"-jar",path+"/talismane-core-5.3.0.jar","--analyse","--sessionId="+valid_langs[lang][1]\
                        ,"--encoding=UTF8","--inFile="+name,"--outFile="+temp2])
                        temp2_file=open(temp2,mode="r",encoding="UTF-8")
                        result=temp2_file.read()
                        temp2_file.close()
                        os.remove(temp2)
                        if inplace or ana:
                            result=result.split("\n")
                            TAL_results=[]
                            for element in result:
                                if len(element.split("\t"))>2:
                                    TAL_results.append(element.split("\t"))
                            if inplace:
                                lecteur = etree.XMLParser(remove_blank_text=True)
                                old_tree=etree.parse(itempath,lecteur)
                                new_elt=etree.SubElement(old_tree.getroot(),"text",attrib={"id":"taggedtext"})
                                for token in TAL_results:
                                    newer_elt=etree.SubElement(new_elt,"w",attrib={"lemma":token[2],"pos":token[3]})
                                    newer_elt.text=token[1]
                                new_name="output/TAL_tagged_"+itempath.split("/")[-1]
                                old_tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
                            elif ana:
                                ana_TAL=""
                                convert_pos=""
                                for token in TAL_results:
                                    if token[3].strip() in TAL_to_TTG.keys():
                                        convert_pos=TAL_to_TTG[token[3].strip()]
                                    else:
                                        convert_pos="NAM"
                                    ana_line=token[1].strip()+"\t"+convert_pos+"\t"+token[2].strip()
                                    ana_TAL=ana_TAL+ana_line+"\n"
                                results.append((itempath,ana_TAL,"Talismane"))
                        else:
                            results.append((itempath,result,"Talismane"))
                    #Cas SpaCy
                    elif prog=="SPC":
                        annotateur = spacy.load(valid_langs[lang])
                        source_file=open(name,mode="r",encoding="UTF-8")
                        source_text=source_file.read()
                        source_file.close()
                        tagged_text = annotateur(source_text)
                        result=""
                        if inplace:
                            lecteur = etree.XMLParser(remove_blank_text=True)
                            old_tree=etree.parse(itempath,lecteur)
                            new_elt=etree.SubElement(old_tree.getroot(),"text",attrib={"id":"taggedtext"})
                            for tok in tagged_text:
                                if tok.pos_!="SPACE":
                                    newer_elt=etree.SubElement(new_elt,"w",attrib={"lemma":tok.lemma_,"pos":tok.pos_})
                                    newer_elt.text=tok.text
                            new_name="output/SPC_tagged_"+itempath.split("/")[-1]
                            old_tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
                        if ana:
                            ana_SPC=""
                            convert_pos=""
                            for tok in tagged_text:
                                if tok.pos_!="SPACE":
                                    if tok.pos_ in UD_to_TTG.keys():
                                        convert_pos=UD_to_TTG[tok.pos_]
                                    else:
                                        convert_pos="NAM"
                                    ana_line=tok.text+"\t"+convert_pos+"\t"+tok.lemma_
                                    ana_SPC=ana_SPC+ana_line+"\n"
                            results.append((itempath,ana_SPC,"SpaCy"))

                        else:
                        #SpaCy produit un objet de type Document comme sortie
                        #il est nécessaire de traiter tous les éléments
                        #de cet objet un par un
                            for tok in tagged_text:
                                if tok.pos_!="SPACE":
                                    result+=tok.text+"\t"+tok.pos_+"\t"+tok.lemma_+"\n"
                            results.append((itempath,result,"SpaCy"))
                    #Cas CoreNLP
                    elif prog=="NLP":
                        subprocess.run(["java","-cp",path+"/stanford-corenlp-3.9.2.jar:"\
                        +path+"/"+valid_langs[lang][1],"-Xmx8g"\
                        ,"edu.stanford.nlp.pipeline.StanfordCoreNLP","-props"\
                        ,"StanfordCoreNLP-"+valid_langs[lang][0]+".properties"\
                        ,"-annotators","tokenize,ssplit,pos,lemma","-file"\
                        ,name,"-outputFormat","text"])
                        #CoreNLP ne nous permet pas de choisir le fichier de sortie
                        #et crée toujours la sortie dans un fichier file.out
                        #dans le dossier depuis lequel CoreNLP est lancé
                        #Il est donc nécessaire de récupèrer le contenu de ce dossier
                        output_filename=name+".out"
                        output_file=open(output_filename,mode="r",encoding="UTF-8")
                        result=output_file.read()
                        output_file.close()
                        os.remove(output_filename)
                        #Pour implémenter NLP, besoin d'aller lire leur format de fichier
                        #où les tokens sont sous cette forme:
                        #[Text=71 CharacterOffsetBegin=35 CharacterOffsetEnd=37 PartOfSpeech=NUM Lemma=71]
                        if inplace or ana:
                            result=result.split("\n")
                            NLP_result=[]
                            for element in result:
                                if element.startswith("["):
                                    element=element.strip("[]")
                                    element=element.split(" ")
                                    NLP_result.append(element)
                            if inplace:
                                lecteur = etree.XMLParser(remove_blank_text=True)
                                old_tree=etree.parse(itempath,lecteur)
                                new_elt=etree.SubElement(old_tree.getroot(),"text",attrib={"id":"taggedtext"})
                                for tok in NLP_result:
                                    lemma=tok[4].split("=")
                                    pos=tok[3].split("=")
                                    text=tok[0].split("=")
                                    newer_elt=etree.SubElement(new_elt,"w",attrib={"lemma":lemma[1],"pos":pos[1]})
                                    newer_elt.text=text[1]
                                new_name="output/NLP_tagged_"+itempath.split("/")[-1]
                                old_tree.write(new_name,encoding="UTF-8",xml_declaration=True,pretty_print=True)
                            elif ana:
                                ana_NLP=""
                                convert_pos=""
                                for tok in NLP_result:
                                    lemma=tok[4].split("=")
                                    pos=tok[3].split("=")
                                    text=tok[0].split("=")
                                    if pos[1] in UD_to_TTG.keys():
                                        convert_pos=UD_to_TTG[pos[1]]
                                    else:
                                        convert_pos="NAM"
                                    ana_line=text[1]+"\t"+convert_pos+"\t"+lemma[1]
                                    ana_NLP=ana_NLP+ana_line+"\n"
                                results.append((itempath,ana_NLP,"CoreNLP"))
                        else:
                            results.append((itempath,result,"CoreNLP"))
                    os.remove(name)

                except Exception as e:
                    raise Exception("Fichier nécessaire au traitement non trouvé")
        else:
            try:
                #Cas TreeTagger
                if prog=="TTG":
                    result=subprocess.check_output([path+'/cmd/tree-tagger-'+valid_langs[lang],itempath])
                    if ana:
                        results.append((itempath,result.decode("UTF-8"),"TreeTagger"))
                    else:
                        results.append((itempath,result.decode("UTF-8"),"TreeTagger"))
                #Cas UDPipe
                elif prog=="UDP":
                    result=subprocess.check_output([path+'/udpipe'\
                    ,"--tokenize","--tag","--parse",models+"/"+valid_langs[lang]\
                    ,itempath])
                    if ana:
                        UDP_result=result.decode("UTF-8")
                        UDP_result=UDP_result.split("\n")
                        UDP_results=[]
                        for element in UDP_result:
                            if len(element.split("\t"))>2:
                                UDP_results.append(element.split("\t"))
                        ana_UDP=""
                        convert_pos=""
                        for token in UDP_results:
                            if token[3].strip() in UD_to_TTG.keys():
                                convert_pos=UD_to_TTG[token[3].strip()]
                            else:
                                convert_pos="NAM"
                            ana_line=token[1]+"\t"+convert_pos+"\t"+token[2]
                            ana_UDP=ana_UDP+ana_line+"\n"
                        results.append((itempath,ana_UDP,"UDPipe"))
                    else:
                        results.append((itempath,result.decode("UTF-8"),"UDPipe"))
                #Cas Talismane
                elif prog=="TAL":
                    #On a besoin de créer un fichier temporaire
                    #car Talismane ne nous permet pas de récupèrer la sortie
                    #depuis la sortie standard
                    temp2=re.sub(" ","",str(datetime.datetime.now()))+"_temp.txt"
                    temp2="./"+temp2
                    subprocess.run(["java","-Xmx1G","-Dconfig.file="+path+"/"+valid_langs[lang][0]\
                    ,"-jar",path+"/talismane-core-5.3.0.jar","--analyse","--sessionId="+valid_langs[lang][1]\
                    ,"--encoding=UTF8","--inFile="+itempath,"--outFile="+temp2])
                    temp2_file=open(temp2,mode="r",encoding="UTF-8")
                    result=temp2_file.read()
                    temp2_file.close()
                    os.remove(temp2)
                    if ana:
                        result=result.split("\n")
                        TAL_results=[]
                        for element in result:
                            if len(element.split("\t"))>2:
                                TAL_results.append(element.split("\t"))
                        ana_TAL=""
                        convert_pos=""
                        for token in TAL_results:
                            if token[3].strip() in TAL_to_TTG.keys():
                                convert_pos=TAL_to_TTG[token[3].strip()]
                            else:
                                convert_pos="NAM"
                            ana_line=token[1].strip()+"\t"+convert_pos+"\t"+token[2].strip()
                            ana_TAL=ana_TAL+ana_line+"\n"
                        results.append((itempath,ana_TAL,"Talismane"))
                    else:
                        results.append((itempath,result,"Talismane"))
                #Cas SpaCy
                elif prog=="SPC":
                    annotateur = spacy.load(valid_langs[lang])
                    source_file=open(itempath,mode="r",encoding="UTF-8")
                    source_text=source_file.read()
                    source_file.close()
                    tagged_text = annotateur(source_text)
                    result=""
                    if ana:
                        ana_SPC=""
                        convert_pos=""
                        for tok in tagged_text:
                            if tok.pos_!="SPACE":
                                if tok.pos_ in UD_to_TTG.keys():
                                    convert_pos=UD_to_TTG[tok.pos_]
                                else:
                                    convert_pos="NAM"
                                ana_line=tok.text+"\t"+convert_pos+"\t"+tok.lemma_
                                ana_SPC=ana_SPC+ana_line+"\n"
                        results.append((itempath,ana_SPC,"SpaCy"))
                    else:
                        for tok in tagged_text:
                            if tok.pos_!="SPACE":
                                result+=tok.text+"\t"+tok.pos_+"\t"+tok.lemma_+"\n"
                        results.append((itempath,result,"SpaCy"))
                #Cas CoreNLP
                elif prog=="NLP":
                    subprocess.run(["java","-cp",path+"/stanford-corenlp-3.9.2.jar:"\
                    +path+"/"+valid_langs[lang][1],"-Xmx8g"\
                    ,"edu.stanford.nlp.pipeline.StanfordCoreNLP","-props"\
                    ,"StanfordCoreNLP-"+valid_langs[lang][0]+".properties"\
                    ,"-annotators","tokenize,ssplit,pos,lemma","-file"\
                    ,itempath,"-outputFormat","text"])
                    full_itempath=itempath.split("/")
                    output_filename=full_itempath[-1]+".out"
                    output_file=open(output_filename,mode="r",encoding="UTF-8")
                    result=output_file.read()
                    output_file.close()
                    os.remove(output_filename)
                    if ana:
                        result=result.split("\n")
                        NLP_result=[]
                        for element in result:
                            if element.startswith("["):
                                element=element.strip("[]")
                                element=element.split(" ")
                                NLP_result.append(element)
                        ana_NLP=""
                        convert_pos=""
                        for tok in NLP_result:
                            lemma=tok[4].split("=")
                            pos=tok[3].split("=")
                            text=tok[0].split("=")
                            if pos[1] in UD_to_TTG.keys():
                                convert_pos=UD_to_TTG[pos[1]]
                            else:
                                convert_pos="NAM"
                            ana_line=text[1]+"\t"+convert_pos+"\t"+lemma[1]
                            ana_NLP=ana_NLP+ana_line+"\n"
                        results.append((itempath,ana_NLP,"CoreNLP"))
                    else:
                        results.append((itempath,result,"CoreNLP"))
            #Si un fichier nécessaire au traitement n'est pas trouvé, on crée une exception
            #pour avetir l'utilisateur
            except Exception as e:
                #raise Exception("Fichier nécessaire au traitement non trouvé")
                raise
