# Documentation du script tag.py - version Docker "lite"
#### En construction, certains éléments pourraient changer

Note: Pour le moment, cette documentation part du principe que vous utilisez un système d'exploitation basé sur Linux. Les commandes et prérequis peuvent être différents sur d'autres systèmes d'exploitation.

## Prérequis

Docker installé, avec la commande:
```
sudo apt install docker
```

Cet outil fait appel aux outils suivants, mais il n'est pas nécessaire de les installer sur votre machine :

-[TreeTagger](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/)

-[UDPipe](http://ufal.mff.cuni.cz/udpipe)

-[Talismane](http://redac.univ-tlse2.fr/applications/talismane.html)

-[SpaCy](https://spacy.io)

-[CoreNLP](https://stanfordnlp.github.io/CoreNLP/)


## Installation et configuration:

Cloner le dépôt Github dans un dossier séparé.

Lors de la première utilisation: ouvrir une ligne de commande dans la copie du dépôt Github et construire l'image Docker avec la commande suivante:

```
docker-compose build
```


### Attention

Cette opération peut prendre un certain temps en fonction de la vitesse de votre connexion Internet, car le script doit télécharger les cinq outils d'étiquetage pour les installer dans l'image. Cependant, cette opération n'est nécessaire qu'une seule fois, tout lancement de l'outil suivant réutilisera les fichiers téléchargés.


## But:
Créer un script utilisable en ligne de commande pouvant lancer un analyseur morphosyntaxique choisi sur un ou plusieurs fichiers, et peut-être éventuellement intégrer un traitement textométrique des données.

Cet outil n'a pas vocation à implémenter toutes les options de chaque programme. Il est donc possible que certaines fonctionnalités de chacun des analyseurs morphosyntaxiques ne soient pas disponibles.

Le but de cette version allégée de l'installation est de diminuer le temps nécessaire pour l'installation de l'outil en n'installant qu'une partie des langues que peuvent traiter ces outils. Une liste complète des langues installées est disponible dans la section **Langues Installées**, avec leurs codes ISO-639-3.


## Etat actuel:

Étiquetage avec TreeTagger, UDPipe, Talismane, SpaCy et CoreNLP disponible.

Traitement rudimentaire de fichiers XML-TEI avec TreeTagger, UDPipe, SpaCy, CoreNLP ou Talismane possible

Traitement en place rudimentaire de fichiers XML-TEI avec TreeTagger, CoreNLP, Talismane, UDPipe et SpaCy possible.

Des exemples de chaque type de sortie effectuées par chaque outil sont disponible dans le dossier *exemples*

## Usage:
Déposer les dossier à traiter dans le dossier *input*, qui se trouve dans le dossier *data* du dépôt.

Ensuite, depuis la ligne de commande, dans le dossier contenant le dépôt, démarrer le conteneur Docker avec la commande suivante :

```
docker-compose up -d
```

Une fois le conteneur lancé, entrer la commande suivante pour se mettre à travailler dans le conteneur:

```
docker-compose exec app bash
```

Si la ligne de commande remplace son préfixe par le suivant, vous êtes bien en train de travailler dans le conteneur:

```
root@[12 caractères aléatoires]:
```

Toute commande entrée sera ensuite prise en charge par le conteneur et non par votre machine locale jusqu’à ce que vous quittiez le conteneur.

Pour quitter le conteneur, entrer cette commande:

```
exit
```

Enfin, pour arrêter le conteneur une fois que vous l'avez quitté:

```
docker-compose down
```

Tant que le conteneur n'est pas arrêté, il est toujours possible de le relancer avec la commande

```
docker-compose exec app bash
```

Une fois le conteneur arrêté, il faut le relancer avec la commande suivante pour travailler de nouveau dedans.

```
docker-compose exec app bash
```

Une fois dans le conteneur, l'outil peut être lancé à l'aide de cette commande:

```
python3 tag_d.py entree sortie (--sep) (--norec) (--lang xxx) (--exts .xxx (.xxx,.xxx,...)) (--prog xxxxxx (xxxxxx xxxxxx)) (--inplace) (--clean) (--keepstruct xxx) (--ana)
```

*entree* doit être un fichier ou dossier qui se trouve dans le dossier data/input, et *sortie* est un fichier ou dossier qui sera crée dans le dossier data/output. Dans le deux cas, le chemin d'accès doit être relatif à ces deux dossiers.

Toute commande entre parenthèses est optionnelle. Si une de ces options n'est pas renseignée, le script utilisera les valeurs par défaut indiquées ci-dessous.

Les commandes *entree* et *sortie* doivent être remplacés par leur valeurs, en respectant l'ordre indiqué ci-dessus.

Les *xxx* qui suivent certaines options doivent être remplacés par les choix que vous souhaitez faire, sous le format indiqué ci-dessous.

À l'exception de la commande *--exts*, toute commande qui nécessite un argument ne peut en accepter plus d'un seul.

Si une option n'est pas suivie par des xxx, cela veut dire qu'il n'est pas nécessaire de préciser un argument pour ces options.

Exemple de commande simple:

```
python3 tag_d.py Test Test_sortie.csv
```

Cette commande va lancer le script sur le dossier Test dans le dossier data/input avec les option par défaut précisées ci-dessous et va enregistrer les résultats dans le fichier Test_sortie.csv dans le dossier data/output.

Exemple de commande:

```
python3 tag_d.py Test Test_sortie.csv --norec --lang eng --exts .txt .xml --prog Treetagger SpaCy --inplace
```

Cette commande va lancer le script sur le dossier Test dans le dossier data/input, va enregistrer les résultats dans le fichier Test_sortie.csv dans le dossier data/output, elle va demander au script de ne pas effectuer un parcours récursif, elle précise que la langue à traiter est l'anglais, elle précise que le script ne doit traiter que les fichiers dont l'extension est .txt ou .xml, elle demande au script de lancer les analyseurs TreeTagger et SpaCy, et elle lui indique d'inscrire les résultats dans le fichier si le fichier d'entrée est du XML.


Il est possible d'obtenir un message d'aide résumant toutes ces informations en entrant l'une des deux commandes suivantes:

```
python3 tag_d.py -h
```

ou bien

```
python3 tag_d.py --help
```

### Arguments:


####  entree (argument obligatoire)

Chemin d’accès (relatif au dossier data/input) au fichier ou dossier dont les textes doivent être analysés.

####  sortie (argument obligatoire)

Chemin d’accès relatif au dossier data/output où les données doivent être stockées. (Toute éventuelle extension de fichier sera retirée si l'option -sep est ajoutée, et le chemin d'accès sera utilisé comme chemin d'accès du dossier à créer).
 Les formats de sortie recommandés sont .txt et .csv

####  --sep (argument optionnel)

Change le format de sortie: par défaut, touts les résultats seront concaténés en un fichier avec un séparateur entre chaque fichier. (voir **Exemple de Séparateur**) Si cette option est ajoutée, un nouveau dossier sera créé qui contiendra une copie de l'arborescence du dossier fourni en entrée qui contiendra les fichiers traités, nommés selon le format suivant: "original.xxx_sortie_outil.txt" (par exemple: fichier1.txt_sortie_TreeTagger.txt) .

####  --norec (argument optionnel)

Indique au script de ne pas effectuer un parcours récursif. Par défaut, le script parcourt récursivement tout dossier trouvé dans le dossier d'entrée. Avec cette option, ce parcours récursif n'aura pas lieu. (aucun effet si l’entrée est un fichier isolé ou un dossier sans sous-dossiers)

####  --lang (argument optionnel)

Indique quelle langue doit être utilisée pour l'étiquetage. La langue doit être indiquée sous la forme de son code ISO-639-3. (Un outil permettant de rechercher ce code se trouve à l'adresse suivante : [Ici](https://iso639-3.sil.org/code_tables/639/data). Un tableau présentant les codes ISO-639-3 des langues installées par l'outil se trouve aussi ci-dessous dans la section **Informations supplémentaires**.
 *(Valeur par défaut:fra)*

#### --exts (argument optionnel)

 Indique la ou les extensions des fichiers qui doivent être traités. Chaque extension doit être séparée par un espace. Il est nécessaire de préciser le point devant le code de chaque extension (écrire .csv ou .txt par exemple).
 *(Valeur par défaut:.txt)*

#### --prog (argument optionnel)

 Indique quel étiqueteur morphosyntaxique le script doit utiliser. Les commandes pour appeler chacun des étiqueteurs sont disponibles dans leurs fichiers d'information dans le dossier *Informations sur chaque outil*. Un tableau contenant les commandes de tous les étiqueteurs est aussi disponible ci-dessous dans la section **Informations supplémentaires**. Il est possible d'entrer plusieurs programmes. Dans ce cas, le script va les lancer les uns après les autres et inscrire les résultats des différents outils dans le même fichier.
 *(Valeur par défaut:Treetagger)*

#### --inplace (argument optionnel)

  Indique au script de représenter les résultats de l'analyse de fichiers XML directement dans une copie du fichier XML lui-même sous forme d'un nouvel élément ```<text>```. Cette copie se trouvera dans le dossier *output*, et son nom sera *XXX_tagged_YYY.xml*, XXX étant l'outil utilisé et YYY étant le nom original du fichier. Par défaut, le script écrit les résultats dans un nouveau fichier sans aucun formatage XML. Cette option n'a d'effet que sur les fichiers XML, tout autre type de fichier sera toujours traité normalement. Le script reste bien sûr capable de traiter plusieurs fichiers de type identique ou différents, même si cette option est activée. Renseigner cette option indiquera aussi au script d'ajouter *.xml* à la liste des extensions qui doivent être analysées, il n'est donc pas nécessaire de préciser l'option ```--exts .xml``` si cette option est activée.

  Avertissement: Cette analyse ne fonctionne que sur des fichiers XML-TEI.

#### --keepstruct (arguement optionnel)

  Indique au script d'inscrire les résultats de l'analyse de fichiers XML directement dans une copie du fichier XML, en essayant de conserver la structure originale autant que possible. Cette copie se trouvera dans le dossier *output*, et son nom sera *XXX_XML_tagged_YYY.xml*, XXX étant l'outil utilisé et YYY étant le nom original du fichier. Il est nécessaire de préciser une option après cet argument: ce doit être une balise XML présente dans le texte. Cette balise sera utilisée comme balise minimum: toute balise incluse dans la balise indiquée sera ignorée lors de l'étiquetage, seul le texte brut sera récupéré et traité. Par défaut, le script écrit les résultats dans un nouveau fichier sans aucun formatage XML. Cette option n'a d'effet que sur les fichiers XML, tout autre type de fichier sera toujours traité normalement. Le script reste bien sûr capable de traiter plusieurs fichiers de type identique ou différents, même si cette option est activée. Si cette option ainsi que l'option "inplace" sont activées, celle-ci sera la seule lancée. Le script tente aussi de séparer le texte analysé en phrase si possible, phrases qui sont représentées sous la forme d'éléments XML ```<s></s>```. Cependant, tous les outils ne permettent pas ce traitement: les options de traitement des phrases intégrées à SpaCy et TreeTagger ne convenaient pas aux demandes du script, donc la version implémentée dans le script n'est pas capable de séparer les phrases, et inscrit tout le contenu de chaque balise dans une seule balise ```<s>```. Cette information est présentée ci-dessous dans le tableau **Tableau récapitulatif des langues traitées par chaque étiqueteur**.

  Avertissement: Cette analyse ne fonctionne que sur des fichiers XML-TEI.


  Avertissement: Ajouter cette option allonge grandement le temps de traitement car chaque balise est traitée individuellement au lieu de traiter tout le texte en une fois, et le temps de chargement de chacun des outils peuvent devenir très long dans ce type de cas.

#### --clean (argument optionnel)

  Indique au script de ne pas ajouter de séparateurs entre les résultats de l'analyse de chaque fichier. Normalement, le script ajoute le séparateur indiqué ci-dessous dans la partie **Exemple de Séparateur** entre la sortie de chaque fichier afin d'améliorer la lisibilité du résultat final. Cette option permet de désactiver cet ajout afin d'avoir des données plus "propres", qui peuvent être ensuite réutilisées plus facilement.

#### --ana (argument optionnel)

  Indique au script de lancer la partie analyse de AnaText sur les fichiers donnés en entrée, et compile les résultats dans un fichier compressé qui apparaît dans le dossier de sortie habituel. Ne fonctionne pas si l'option --keepstruct ou l'option --inplace est activée. Si un programme de traitement autre que TreeTagger est choisi, les résultats seront convertis en résultats TreeTagger afin d'être traités par AnaText

## Informations supplémentaires

### Mini-serveur Apache

  Cet outil inclut maintenant un serveur Apache local afin d'afficher les fichiers qui résultent du lancement d'AnaText sur les fichiers fournis en entrée. Pour accèder à ce serveur, il est d'abord nécessaire de renseigner le port que doit utiliser ce serveur dans le fichier *.env.dist* en complètant la ligne PORTAPACHE par le port auquel le serveur doit se relier, puis de renommer ce fichier en *.env*. Le serveur est en suite accessible tant que le conteneur Docker est lancé en tapant ceci dans la barre d'adresse de tout navigateur Internet:
  *http://localhost:XXX/*, en remplaçant les XXX par l'identifiant du port renseigné selon les instructions ci-dessus.
  Afin d'afficher des fichiers sur ce serveur, il faut lancer l'outil avec l'option "ana" renseignée.

### Exemple de Séparateur:

```
|\===============================================================================|
Sortie yyy du fichier "x.xxx"
|\===============================================================================|
```

*yyy* indique l'outil utilisé pour l'étiquetage du texte.

Ce séparateur n'est pas inscrit si l'option 'clean' est activée.

### Langues installées:

| Langue            | Code ISO-639-3 |
|-------------------|----------------|
| Français          |fra             |
| Anglais           |eng             |
| Allemand          |deu             |
| Espagnol          |spa             |
| Italien           |ita             |
| Latin             |lat             |
| Grec moderne      |ell             |
| Grec ancien       |grc             |
| Vieux Français    |fro             |
| Portugais         |por             |

### Tableau des commandes des étiqueteurs

| Etiqueteurs | Commandes                               |
|-------------|-----------------------------------------|
| TreeTagger  | TreeTagger, Treetagger, TTG, treetagger |
| SpaCy       | SpaCy, Spacy, SPC, spacy                |
| CoreNLP     | CoreNLP, corenlp, NLP                   |
| UDPipe      | UDPipe, UDpipe, UDP, udpipe             |
| Talismane   | Talismane, talismane, TAL               |

### Tableau récapitulatif des langues traitées par chaque étiqueteur

Note : Certains de ces étiqueteurs sont capables de traiter d'autres langages si installés de façon indépendante, mais le choix de limiter le nombre de langues traitées à été fait afin de réduire la taille de l'installation de l'application.

| Etiqueteurs | Langues                                          | Capable de séparer les phrases |
|-------------|--------------------------------------------------|--------------------------------|
| TreeTagger  | fra, eng, deu, spa, ita, lat, ell, grc, fro, por | Non                            |
| SpaCy       | fra, eng, deu, spa, ita, ell, por                | Non                            |
| CoreNLP     | fra, eng, deu, spa                               | Oui                            |
| UDPipe      | fra, eng, deu, spa, ita, lat, ell, grc, fro, por | Oui                            |
| Talismane   | fra, eng                                         | Oui                            |

## Liste des erreurs et de leur signification (liste non exhaustive)

#### Langage incorrect ou non supporté par (programme):

Le code ISO-639-3 entré ne correspond pas à un langage que le programme choisi est capable de traiter. Un outil pour trouver le code ISO-639-3 d'une langue est disponible à cette [adresse](https://iso639-3.sil.org/code_tables/639/data).

Le tableau **Tableau récapitulatif des langues traitées par chaque étiqueteur** ci-dessus liste les langues traitées par chaque outil.

#### Programme non supporté ou commande de programme incorrect:

La commande de programme entrée ne correspond pas à un programme implémenté dans l'outil. Une liste des commandes valides se trouve plus haut dans ce document.

#### Dossier déjà existant

Cette erreur indique qu'il existe déjà un dossier dans le dossier de sortie qui porte le nom du dossier que vous souhaitiez créer avec l'option *--sep*. Afin d'éviter tout écrasement de données accidentel, le script fait le choix de s'interrompre sans écrire quoi que ce soit dans ce cas. Il suffit de choisir un autre nom pour le dossier de sortie pour corriger cette erreur.

#### Etiquette 'text' non trouvée

Cette erreur indique que le fichier XML donné comme entrée ne possède pas de balise *text* qui délimite ce qui doit être analysé. Pour le moment, ce script n'est capable d'analyser un fichier XML que si il possède un élément *text* contenant ce qui doit être analysé. Cette erreur peut aussi indiquer que le fichier fourni en entrée n'est pas un fichier XML-TEI, auquel cas il ne peut être traité par l'outil.
