Dès les premières heures de notre publication, nous avons annoncé le
chef-d'oeuvre du poëte florentin comme devant figurer en première ligne
parmi les joyaux de notre modeste écrin. Nous avons voulu, au début,
donner accès à tous les ouvrages consacrés par le temps et par
l'admiration universelle. Un succès constant pendant quatre longues et
parfois difficiles années, nous a prouvé que nous nous étions très
rarement trompé sur la valeur des écrits dont nous tentions la remise
au jour. Si des impatiences honorables gourmandaient les éditeurs de la
_Bibliothèque Nationale_ de n'avoir pas toujours obéi à un système de
chronologie littéraire qui ne nous paraissait pas si logique qu'on
semblait le croire, nous avons maintes fois pris à tâche de rassurer
ces impatiences dans la mesure de ce qui nous paraissait sage et
raisonnable, et nous nous estimons heureux de leur donner enfin
satisfaction en inaugurant la cinquième année d'existence de notre
collection par la publication du poëme le plus grandiose qu'ait produit
le génie humain, sans en excepter l'_Iliade_ et l'_Énéide_.
