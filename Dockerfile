FROM python:3.7
WORKDIR /
COPY app/ /app
RUN apt update
#Installation d'un JRE pour Talismane et CoreNLP
RUN apt -y install default-jre
RUN mkdir /app/outils
#Installation d'UDPipe et de ses modèles
RUN cd /app/outils && wget https://github.com/ufal/udpipe/releases/download/v1.2.0/udpipe-1.2.0-bin.zip && unzip udpipe-1.2.0-bin.zip && rm udpipe-1.2.0-bin.zip && mv udpipe-1.2.0-bin udpipe && cd ./udpipe && rm -r bin-linux32 && rm -r bin-osx && rm -r bin-win32 && rm -r bin-win64 && mkdir models && cd ./models && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/ancient_greek-perseus-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/english-ewt-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/german-gsd-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/french-sequoia-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/greek-gdt-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/italian-isdt-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/latin-ittb-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/old_french-srcmf-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/portuguese-bosque-ud-2.5-191206.udpipe && wget -t 0 https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/spanish-ancora-ud-2.5-191206.udpipe
#Installation de CoreNLP et de ses modèles
RUN cd /app/outils && wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-10-05.zip && unzip stanford-corenlp-full-2018-10-05.zip && rm stanford-corenlp-full-2018-10-05.zip && mv stanford-corenlp-full-2018-10-05 corenlp && cd ./corenlp && wget http://nlp.stanford.edu/software/stanford-english-corenlp-2018-10-05-models.jar && wget http://nlp.stanford.edu/software/stanford-french-corenlp-2018-10-05-models.jar && wget http://nlp.stanford.edu/software/stanford-german-corenlp-2018-10-05-models.jar && wget http://nlp.stanford.edu/software/stanford-spanish-corenlp-2018-10-05-models.jar
RUN pip3 install -r /app/requirements.txt
#Télechargement des modèles de SpaCy
RUN python3 -m spacy download en_core_web_sm
RUN python3 -m spacy download de_core_news_sm
RUN python3 -m spacy download fr_core_news_sm
RUN python3 -m spacy download es_core_news_sm
RUN python3 -m spacy download pt_core_news_sm
RUN python3 -m spacy download it_core_news_sm
RUN python3 -m spacy download el_core_news_sm
#Installation de Talismane
RUN cd /app/outils && mkdir talismane && cd ./talismane && wget https://github.com/joliciel-informatique/talismane/releases/download/v5.3.0/talismane-distribution-5.3.0-bin.zip && unzip talismane-distribution-5.3.0-bin.zip && rm talismane-distribution-5.3.0-bin.zip && wget https://github.com/joliciel-informatique/talismane/releases/download/v5.2.0/englishLanguagePack-5.2.0.zip && wget https://github.com/joliciel-informatique/talismane/releases/download/v5.2.0/frenchLanguagePack-5.2.0.zip && wget https://github.com/joliciel-informatique/talismane/releases/download/v5.2.0/talismane-en-5.2.0.conf && wget https://github.com/joliciel-informatique/talismane/releases/download/v5.2.0/talismane-fr-5.2.0.conf
#Nettoyage des fichiers de Talismane pour éviter certains bugs
RUN cd /app && python3 clean.py ./outils/talismane/talismane-en-5.2.0.conf && python3 clean.py ./outils/talismane/talismane-fr-5.2.0.conf
#Installation de TreeTagger et de ses modèles
RUN cd /app/outils && mkdir treetagger && cd ./treetagger && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.2.tar.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagger-scripts.tar.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/install-tagger.sh && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/greek.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/latin.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/portuguese.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/ancient-greek.par.gz && wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/old-french.par.gz && sh install-tagger.sh
WORKDIR /app
